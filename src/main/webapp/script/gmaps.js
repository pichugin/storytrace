var map;
var geocoder;

function gmapInit() {
	if (GBrowserIsCompatible()) {
		map = new GMap2(document.getElementById("map_canvas"));
		map.setCenter(new GLatLng(43.75, -79.40), 13);
		map.setUIToDefault();
		geocoder = new GClientGeocoder();
	}
}

function centerMap(newCenter) {
	map.setCenter(newCenter, 13);
}

//Change zoom / pan so as to fit all the points on the map.
function fitAllPoints(){
	/* TODO: Implement this function */
}

function coordinatesToPoint(lat, lng) {
	return new GLatLng(lat, lng);
}


// This is just to show the marker info popup when moving mouse over an item in the result list.
var markers = new Array();

function addPoint(id, point, info) {
	//TODO:
	//Keep a static list of all the existing points, so as not to 
	//add the same point twice.
	
	var marker = new GMarker(point);
	GEvent.addListener(marker, "click", function() {
		marker.openInfoWindowHtml(info);
	});
	map.addOverlay(marker);
	markers[id] = marker;
	markers[id + "-info"] = info;
	//centerMap(point);
}

function removePoints(){
	map.clearOverlays();
}

function showMarkerInfo(id) {
	markers[id].openInfoWindowHtml(markers[id + "-info"]);
}


/*
function parseJSON(json, indent) {
	var s = ""; 
	for (var name in json) {
		if (typeof(json[name]) == 'object') {
			s += indent + name + ":\n" + parseJSON(json[name], indent + "\t");
		} else {
			s += indent + name + ":" + json[name] + "\n";
		}
	}
	return s;
}
*/

function jsonToXml(json) {
	return "<address>\n\t" + jsonToXmlRecursively(json) + "\n</address>";  
}

function jsonToXmlRecursively(json) {
	var s = ""; 
	for (var name in json) {
		if (typeof(json[name]) == 'object') {
			s += "\n" + jsonToXmlRecursively(json[name]);
		} else {
			var tagName = generateTagName(name);
			s += "\t<" + tagName + ">" + json[name] + "</" + tagName + ">\n";
		}
	}
	return s;
}

function generateTagName(name) {
	// XML tag name cannot start with a number
	if (name[0] >= '0' && name[0] <= '9') {
		return "number" + name;
	}
	// Lowercase the first character and return the new name:
	return name.substring(0, 1).toLowerCase() + name.substring(1);
}


var foundLocations = new Array();     // In XML format
var foundLocationsJSON = new Array(); // In JSON format

function removeLocations() {
	foundLocations = new Array(); 
	foundLocationsJSON = new Array();
	removePoints();
}

function getAddress(addressString, callback) {
	var geo = new GClientGeocoder();
	geo.getLocations(addressString, function (result) {
		map.clearOverlays();
		if (result.Status.code == G_GEO_SUCCESS) {
			for (var i = 0; i < result.Placemark.length; i++) {
				foundLocations[i] = jsonToXml(result.Placemark[i]); //parseJSON(result.Placemark[i], "");
				foundLocationsJSON[i] = result.Placemark[i];
			}
		} else {
			var reason="Code "+result.Status.code;
			if (reasons[result.Status.code]) {
				reason = reasons[result.Status.code];
			} 
			alert('Could not find "' + addressString + '" ' + reason);
		}
		if (callback) {
			callback();
		}
	});
	return false; 
}

function place(lat,lng) {
    map.clearOverlays(); 
    var point = new GLatLng(lat,lng);
    map.setCenter(point,14); 
    map.addOverlay(new GMarker(point));
}

var reasons=[];

reasons[G_GEO_SUCCESS]            = "Success";
reasons[G_GEO_MISSING_ADDRESS]    = "Missing Address: The address was either missing or had no value.";
reasons[G_GEO_UNKNOWN_ADDRESS]    = "Unknown Address:  No corresponding geographic location could be found for the specified address.";
reasons[G_GEO_UNAVAILABLE_ADDRESS]= "Unavailable Address:  The geocode for the given address cannot be returned due to legal or contractual reasons.";
reasons[G_GEO_BAD_KEY]            = "Bad Key: The API key is either invalid or does not match the domain for which it was given";
reasons[G_GEO_TOO_MANY_QUERIES]   = "Too Many Queries: The daily geocoding quota for this site has been exceeded.";
reasons[G_GEO_SERVER_ERROR]       = "Server error: The geocoding request could not be successfully processed.";

if (GBrowserIsCompatible()) {
	//TODO: wrap all the JS above into this condition
}
