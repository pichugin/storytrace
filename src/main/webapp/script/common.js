
function isIE() {
	var browser=navigator.appName;
	if (browser=="Microsoft Internet Explorer"){
		return true;
	}
	return false;
}


function executeIfEnter(evt, funcName) {
	var btnCode = (navigator.appName == "Netscape") ? evt.which : evt.keyCode;
	if (btnCode == 13) {
		eval(funcName);
	}
}

function executeIfCtrlEnter(evt, funcName) {
	var btnCode = (navigator.appName == "Netscape") ? evt.which : evt.keyCode;
    if (btnCode == 13 && evt.ctrlKey) {
    	eval(funcName);
    }
}


//TODO: Replace all $.ajax({}) calls with ajaxPost(..) and perhaps, merge ajaxGet and ajaxPost into one function.

function ajaxGet(url, params, resultContainerId) {
	$.ajax({
		type: 'GET',
		url: url,
		data: params,
		contentType: "application/x-www-form-urlencoded;charset=UTF-8",
		dataType: "html",
		success: function(data) {
			$("#"+resultContainerId).html(data);
		}
	});
}

function ajaxPost(url, params, resultContainerId) {
	$.ajax({
		type: 'POST',
		url: url,
		data: params,
		contentType: "application/x-www-form-urlencoded;charset=UTF-8",
		dataType: "html",
		success: function(data) {
			$("#"+resultContainerId).html(data);
		}
	});
}
