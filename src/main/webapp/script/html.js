
var html = new HTML();

function HTML() {
	
	this.generateSelect = function (withEmptyOption, disabled) {
		var select = document.createElement("SELECT");
		if (disabled == true) { 
			select.disabled = true;
		}
		if (withEmptyOption == true) {
			var option = document.createElement("option");
			option.value = "";
			option.label = "";
			select.appendChild(option);
		}
		return select;
	};
	
	this.generateDiv = function (id) {
		var div = document.createElement("DIV");
		div.setAttribute("id", id);
		return div;
	};
	
	this.removeChildren = function (obj) {
		var childs = obj.getElementsByTagName("*");
		for (i = 0; i < childs.length; i++) {
			obj.removeChild(childs[i]);
		}
	};
	
}