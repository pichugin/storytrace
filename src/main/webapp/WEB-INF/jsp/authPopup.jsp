<%@ include file="/WEB-INF/jsp/includes.jsp" %>

<div id="authPopupId"></div>

<c:if test="${empty(userSession) || userSession.invalid}">
	<script type="text/javascript">
		function authPopup(strict) {
			
			<%-- A workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore! --%>
			//$("#authPopupId:ui-dialog").dialog("destroy");

			var popup = $("#authPopupId").dialog({
				autoOpen : true,
				height : 260,
				width  : 450,
				modal  : true,
				resizable : false,
				draggable : true,
				title: '<fmt:message key="log_in"/>'
			});
			if (strict) { // Here we decide what we do upon login cancelation.
				popup.bind("dialogclose", function(event, ui) { 
					window.location.href = "<c:url value="/story/list"/>"; 
				});
			}

			$.ajax({
				type: 'GET',
				url: '<c:url value="/user/login"/>' <c:if test="${!empty(signonForwardAction)}">+ "?signonForwardAction=" + escape("${signonForwardAction}")</c:if>,
				contentType: "application/x-www-form-urlencoded;charset=UTF-8",
				success: function(data) {
					$('#authPopupId').html(data);
				}
			});
			
		}
	</script>	
</c:if>
