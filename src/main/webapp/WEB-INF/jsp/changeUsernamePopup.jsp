<%@ include file="/WEB-INF/jsp/includes.jsp" %>

<div id="changeUsernamePopupId"></div>

<script type="text/javascript">
	function changeUsernamePopup() {
		
		var popup = $("#changeUsernamePopupId").dialog({
			autoOpen : true,
			height : 270,
			width  : 480,
			modal  : true,
			resizable : false,
			draggable : true,
			title: '<fmt:message key="change_username"/>'
		});
		
		$.ajax({
			type: 'GET',
			url: '<c:url value="/user/changeUsername"/>',
			contentType: "application/x-www-form-urlencoded;charset=UTF-8",
			success: function(data) {
				$('#changeUsernamePopupId').html(data);
			}
		});
		
	}
</script>