<%@ include file="/WEB-INF/jsp/includes.jsp" %>

<%@ include file="/WEB-INF/jsp/header.jsp" %>

<div id="main">

<div class="contactInfo">
	<h3 style="margin-top:5px;"><fmt:message key="keep_in_touch"/></h3>
	
	<ul>
		<%-- TODO: Do we want to display the email? Can we hide it somehow? --%>
		<%-- 
		<li>Email: <a href="mailto:info@storytrace.com">info@storytrace.com</a></li>
		--%>
		<li><fmt:message key="feedback"/>: <a href="#" onclick="feedbackPopup(); return false;" style="text-transform:lowercase;"><fmt:message key="click_here"/></a></li>
		<li><fmt:message key="twitter"/>: <a href="http://twitter.com/storytrace">twitter.com/storytrace</a></li>
	</ul>
</div>

<h1><fmt:message key="about"/></h1>
<p class="aboutParagraph">
	<fmt:message key="about_message"/>
	<c:if test="${!userSession.valid}">
		<fmt:message key="about_message_not_a_member"/> 
		<a href='<c:url value="/user/edit"/>'><fmt:message key="sign_up"/></a> 
		<fmt:message key="about_message_signup"/>
	</c:if>
</p>

<div style="padding-left:15px; padding-bottom:40px;">
	<h3><fmt:message key="share"/></h3>
	<p class="aboutParagraph">
		<fmt:message key="about_message_share"/>
	</p>
	
	<h3><fmt:message key="rate"/></h3>
	<p class="aboutParagraph">
		<fmt:message key="about_message_rate"/>
	</p>
	
	<h3><fmt:message key="discover"/></h3>
	<p class="aboutParagraph">
		<fmt:message key="about_message_discover"/>
	</p>
</div>

</div>
<%@ include file="/WEB-INF/jsp/footer.jsp" %>