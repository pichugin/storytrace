<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<td>
	<c:forEach begin="1" end="10" step="1" varStatus="status"> 
		<input type="radio" name="${ratingName}" class="star" value="${status.index}" <c:if 
			test="${status.index == ratingValue}">checked="checked"</c:if> <c:if 
			test="${ratingReadOnly}">disabled="disabled"</c:if> />
	</c:forEach> 
</td>