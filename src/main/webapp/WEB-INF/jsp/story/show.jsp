<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%@ include file="/WEB-INF/jsp/includes.jsp" %>

<?xml version="1.0"?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

<head>
	<link rel="stylesheet" href="<c:url value="/styles/main.css"/>" type="text/css"/>
	<link rel="stylesheet" href="<c:url value="/styles/new.css"/>" type="text/css"/>
	
	<script src="<c:url value="/script/jquery/jquery-1.4.2.min.js"/>" type="text/javascript"></script> 
	<script src="<c:url value="/script/jquery/ui/jquery.ui.min.js"/>" type="text/javascript"></script> 
	<link href="<c:url value="/script/jquery/ui/jquery.ui.css"/>" type="text/css" rel="stylesheet"/> 
</head>

<body style="margin:0; padding:0;">

<%@ include file="/WEB-INF/jsp/authPopup.jsp" %>
<%@ include file="/WEB-INF/jsp/story/vote_js.jsp" %>

<div class="hoverPane voteContainer">
	<div style="float:top; display: block">
		&nbsp;
		<a href="${story.url}" class="ui-icon ui-icon-closethick" style="float:right;"></a>
		<a href="<c:url value="/story/list"/>" class="ui-icon ui-icon-arrowreturnthick-1-w" style="float:right;"></a>
	</div>
	<div id="vote-story-${story.id}" style="float:left; display: block">
		<%@ include file="/WEB-INF/jsp/story/vote.jsp" %>
	</div>
</div>

<iframe src="${story.url}" width="100%" height="100%" frameBorder="0">
	<p><fmt:message key="err.iframe"/></p>
</iframe>


</body>

</html>