<%@ include file="/WEB-INF/jsp/includes.jsp" %>

<%@ include file="/WEB-INF/jsp/header.jsp" %>

<div id="main">

<script type="text/javascript">
	function searchStory() {
		var queryValue = $.trim($("#searchQueryId").val());
		if (queryValue != "") {
			window.location.href = '<c:url value="/story/search"/>?query=' + queryValue;
		}
	}
</script>

<div style="float:right; width:300px; padding-top:13px;">
	<%--form:input path="query" id="searchQueryId" cssClass="formInput"/--%>
	<input id="searchQueryId" type="text" name="query" value="${query}" style="float:left; width:200px;" onkeyup="executeIfEnter(event, 'searchStory()');"/>
	<a href="#" class="searchStory" style="float:right;" onclick="searchStory()"><fmt:message key="search"/></a>
</div>

<h1><fmt:message key="latest_links"/></h1>

<a href="<c:url value="/story/create"/>" class="addStory">
	<fmt:message key="add"/>
</a>

<ul class="sortBy">
	<li class="sortByItem"><fmt:message key="show"/>:</li>
	<li class="sortByItem">
		<a href="<c:url value="/story/list?sort=days"/>" <c:if test='${(param.sort eq "days" || param.sort == null) && param.show == null && param.query == null}'>class="sortByCurrent"</c:if>>
			<fmt:message key="latest"/>
		</a>
	</li>
	<li class="sortByItem">
		&nbsp;
	</li>
	<li class="sortByItem">
		<a href="<c:url value="/story/list?sort=vote&days=1"/>" <c:if test='${param.days eq "1"}'>class="sortByCurrent"</c:if>>
			<fmt:message key="top_in_24_hrs"/>
		</a>
	</li>
	<li class="sortByItem">
		<a href="<c:url value="/story/list?sort=vote&days=7"/>" <c:if test='${param.days eq "7"}'>class="sortByCurrent"</c:if>>
			7 <fmt:message key="days"/>
		</a>
	</li>
	<li class="sortByItem">
		<a href="<c:url value="/story/list?sort=vote&days=30"/>" <c:if test='${param.days eq "30"}'>class="sortByCurrent"</c:if>>
			30 <fmt:message key="days"/>
		</a>
	</li>
	<li class="sortByItem">
		&nbsp;
	</li>
	<c:if test="${userSession.valid}">
	<li class="sortByItem">
		<a href="<c:url value="/story/list?sort=days&show=bookmarks"/>" <c:if test='${param.show eq "bookmarks"}'>class="sortByCurrent"</c:if>>
			<fmt:message key="my_bookmarks"/>
		</a>
	</li>
	</c:if>
	
	
	<li class="socialNetworkItem">
		<iframe src="http://www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.storytrace.com&amp;layout=button_count&amp;show_faces=true&amp;width=80&amp;action=like&amp;font&amp;colorscheme=light&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:80px; height:21px;" allowTransparency="true"></iframe>
	</li>
	<li class="socialNetworkItem">
		<a href="http://twitter.com/share" class="twitter-share-button" data-url="http://www.storytrace.com" data-text="Love travel adventures?" data-count="horizontal"><fmt:message key="tweet"/></a>
	</li>
	<li class="socialNetworkItem" style="margin-right:13px;">
		<a class="DiggThisButton DiggCompact" href="http://digg.com/submit?url=http%3A//www.storytrace.com"></a>
	</li>
</ul>

<script type="text/javascript">
	flexiPaginationTargetUrl = "<c:url value="/story/ajaxList"/>?sort=${sort}&days=${days}&query=${query}&show=${show}";
</script>

<%@ include file="/WEB-INF/jsp/story/vote_js.jsp" %>
<%@ include file="/WEB-INF/jsp/story/flag_js.jsp" %>

<script>
	//TODO: move to a JS file!
	function bookmark(storyId, img) {
		//TODO: aTag.onclick = null;
		var bookmarkId = $("#bookmark-story-" + storyId).val();
		$.ajax({
			url: "<c:url value="/story/bookmark"/>?storyId=" + storyId + "&bookmarkId=" + bookmarkId,
			type: "GET", 
			success: function(data) {
				data = $.trim(data);
				$("#bookmark-story-" + storyId).val(data);
				if (bookmarkId == null || bookmarkId == "") {
					img.src = "<c:url value="/images/fav.png"/>";
				} else {
					img.src = "<c:url value="/images/fav_gray.png"/>";
				}
			}
		});
	}
</script>

<ul class="resultsList" id="resultsListId">
	<%@ include file="/WEB-INF/jsp/story/ajaxList.jsp" %>
</ul>
	
</div>




<c:set var="additionalHeadContent" value="/WEB-INF/jsp/jqpageflow.jsp"/>
<%@ include file="/WEB-INF/jsp/footer.jsp" %>
