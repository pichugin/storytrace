<script type="text/javascript">
var loginCallback = null;

function vote(storyId, direction, aTag) {
	<c:if test="${!userSession.valid}">
		loginCallback = function(onAuthPopupCloseFunc) {
			voteContinued(storyId, direction, aTag, onAuthPopupCloseFunc);
		};
		authPopup(false);
		return;
	</c:if>
	voteContinued(storyId, direction, aTag);
}

function voteContinued(storyId, direction, aTag, onAuthPopupCloseFunc) {
	aTag.onclick = null;
	$.ajax({ 
		url: "<c:url value="/story/vote"/>?storyId=" + storyId + "&direction=" + direction,
		type: "GET", 
		success: function(data) {
			$("#vote-story-" + storyId).html(data);
			if (typeof(onAuthPopupCloseFunc) == 'function') {
				onAuthPopupCloseFunc();
			}
		}
	});
}
</script>