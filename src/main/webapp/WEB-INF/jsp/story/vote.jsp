<%@ include file="/WEB-INF/jsp/includes.jsp" %>

<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<c:set var="vote" value="${userSession.user.votesMap[story.id]}"/>

<c:choose>
	<c:when test="${empty(vote)}">
		<div style="display: block; height: 25px;">
			<div class="voteButtonContainer">
				<a onclick="vote(${story.id}, 1, this)" class="voteImage voteUpImage"></a>
			</div> 
			<div class="voteCountContainer"><c:out value="${story.voteUp}"/></div>
		</div>
		<div style="display: block; margin: 5px;">
			<fmt:message key="vote"/>
		</div>
		<div style="display: block; height: 25px;">
			<div class="voteButtonContainer">
				<a onclick="vote(${story.id}, -1, this)" class="voteImage voteDownImage"></a>
			</div>
			<div class="voteCountContainer"><c:out value="${story.voteDown}"/></div>
		</div>
	</c:when>
	<c:otherwise>
		<div style="display: block; min-height: 25px;">
			<c:choose>
				<c:when test="${vote.voteDirection > 0}">
					<div class="voteButtonContainer"><a class="voteImage voteUpImageDark"></a></div>
				</c:when>
				<c:otherwise>
					<div class="voteButtonContainer"><a class="voteImage voteUpImageDisabled"></a></div>
				</c:otherwise>
			</c:choose>
			<div class="voteCountContainer"><c:out value="${story.voteUp}"/></div>
		</div>
		<div style="display: block; margin: 4px;">
			<a onclick="vote(${story.id}, 0, this)" style="cursor:pointer;"><fmt:message key="unvote"/></a>
		</div>
		<div style="display: block; height: 25px;">
			<c:choose>
				<c:when test="${vote.voteDirection < 0}">
					<div class="voteButtonContainer"><a class="voteImage voteDownImageDark"></a></div>
				</c:when>
				<c:otherwise>
					<div class="voteButtonContainer"><a class="voteImage voteDownImageDisabled"></a></div>
				</c:otherwise>
			</c:choose>
			<div class="voteCountContainer"><c:out value="${story.voteDown}"/></div>
		</div>
	</c:otherwise>
</c:choose>