<%@ include file="/WEB-INF/jsp/includes.jsp" %>

<div id="confirmDeletePopupId">
	<p style="padding:10px; padding-bottom:20px;">
		<fmt:message key="confirm_delete_story_msg"/>
	</p>
	
	<input type="button" onclick="confirmYes();" value="<fmt:message key="yes"/>" class="button" style="margin:5px;"/>
	<input type="button" onclick="confirmNo();" value="<fmt:message key="no"/>" class="button" style="margin:5px;"/>
</div>

<script type="text/javascript">
	var storyId;
	var popup;
	
	$(document).ready(function() {
		popup = $("#confirmDeletePopupId").dialog({
			autoOpen : false,
			height : 170,
			width  : 400,
			modal  : true,
			resizable : false,
			draggable : true,
			title: '<fmt:message key="confirm_delete"/>'
		});
	});

	function confirmDelete(id) {
		storyId = id;
		$("#confirmDeletePopupId").dialog('open');
	}

	function confirmYes() {
		<c:url value="/story/delete" var="url">
			<c:param name="redirect" value="/main/${sessionScope.actionPath}"/>
		</c:url>
		document.location.href = '<c:out value="${url}"/>&storyId='+storyId;
	}

	function confirmNo() {
		$("#confirmDeletePopupId").dialog('close');
	}
</script>