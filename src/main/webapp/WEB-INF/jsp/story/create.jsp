<%@ include file="/WEB-INF/jsp/includes.jsp" %>
<%@ include file="/WEB-INF/jsp/header.jsp" %>

<c:if test="${empty(userSession) || userSession.invalid}">
	<script type="text/javascript">
		$(function() {
			authPopup(true);
		});
	</script>	
</c:if>


<div id="main">

<h1><fmt:message key="add_link"/></h1>

<form:form modelAttribute="story" method="POST">

<form:hidden path="id"/>
<table class="form" style="padding:10px; padding-bottom: 15px;">
	<tr>
		<td class="formLabel"><fmt:message key="url"/>:</td>
		<td><form:input path="url" cssClass="formInputVeryWide" maxlength="${maxUrlLength + 50}" onkeyup="loadTitle(this.value);"/></td>
	</tr>
	<tr>
		<td></td>
		<td><form:errors path="url" cssClass="errors"/></td>
	</tr>
	
	
	<tr>
		<td class="formLabel"><fmt:message key="title"/>:</td>
		<td><form:input path="title" id="pageTitle" cssClass="formInputWide" maxlength="${maxTitleLength + 50}" onkeyup="titleKeypress();"/></td>
	</tr>
	<tr>
		<td></td>
		<td><form:errors path="title" cssClass="errors"/></td>
	</tr>
	
	
	<tr>
		<td class="formLabel"><fmt:message key="description"/>:</td>
		<td><form:textarea path="description" id="description" cssClass="formInputVeryWide formInputHigh"/></td>
	</tr>
	<tr>
		<td></td>
		<td class="errors">
			<form:errors path="description" cssClass="errors"/>
		</td>
	</tr>
	<tr>
		<td></td>
		<td>
			<span id="descriptionLimitText"></span>
		</td>
	</tr>
</table>
<br/>


<div style="padding-left:15px; padding-bottom:40px;">
	<input type="button" value='<c:choose><c:when test="${empty story.id}"><fmt:message key="add"/></c:when><c:otherwise><fmt:message key="save"/></c:otherwise></c:choose>' class="button" onclick="addStory(this);">
	<input type="button" value='<fmt:message key="cancel"/>' class="button" onclick="document.location.href = '<c:url value="/"/>'">
</div>

<script type="text/javascript">
var currentRequestNumber = 0;
var titleEnteredManually = false;

function titleKeypress() {
	titleEnteredManually = (document.getElementById('pageTitle').value.trim() != "");
}

function loadTitle(urlToGet) {
	if (titleEnteredManually) {
		return;
	}
	currentRequestNumber += 1;
	var request = currentRequestNumber;
	$.ajax({
		type: 'GET',
		url: '<c:url value="/story/getTitle"/>',
		data: "url=" + urlToGet,
		contentType: "application/x-www-form-urlencoded;charset=UTF-8",
		dataType: "html",
		success: function(data) {
			loadTitleSuccess(data.trim(), request);
		}
	});
}

function loadTitleSuccess(title, requestNo) {
	//This makes sure the requests get executed in the sequence 
	//in which they're called:
	if (requestNo >= currentRequestNumber) {
		document.getElementById('pageTitle').value = title;
	}
}





function addStory(addButton) {
	<c:if test="${empty(userSession) || userSession.invalid}">
		loginCallback = function(onAuthPopupCloseFunc) {
			addStoryContinued(addButton);
		};
		authPopup(false);
		return;
	</c:if>
	addStoryContinued(addButton);
}

function addStoryContinued(addButton) {
	addButton.onclick = null;
	document.forms[0].submit();
}
</script>
</form:form>
	
</div>



<c:set var="additionalHeadContent" value="/WEB-INF/jsp/story/descInputLimit_js.jsp"/>

<%@ include file="/WEB-INF/jsp/footer.jsp" %>