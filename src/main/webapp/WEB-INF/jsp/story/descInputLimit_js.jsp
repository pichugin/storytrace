<%@ include file="/WEB-INF/jsp/includes.jsp" %>

<script src="<c:url value="/script/jquery/inputlimiter/jquery.inputlimiter.js"/>" type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#description').inputlimiter({
			limit: 500,
			boxId: 'descriptionLimitText',
			limitTextShow: false,
			remTextHideOnBlur: false,
			boxAttach: false
		});
	});
</script>