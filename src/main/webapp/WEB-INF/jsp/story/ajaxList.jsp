<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>

<%@ include file="/WEB-INF/jsp/includes.jsp" %>

	<c:forEach var="story" items="${stories}">
	<li class="resultsList">
		<div id="vote-story-${story.id}" class="voteContainer">
			<%@ include file="/WEB-INF/jsp/story/vote.jsp" %>
		</div>
		<div class="resultImage">
			<%-- <a href="<c:url value="/story/show?id=${story.compoundId}"/>">  --%>
			<a href="${story.url}">
				<c:choose>
					<c:when test="${empty(story.thumbnailUrl)}">
						<img src="<c:url value="/images/thumbnail_default.jpg"/>" alt="Story Screenshot" class="resultImage"/>
					</c:when>
					<c:otherwise>
						<img src="<c:url value="/thumbnails/${story.thumbnailUrl}.jpg"/>" alt="Story Screenshot" class="resultImage"/>
					</c:otherwise>
				</c:choose>
			</a>
		</div>
		<div class="resultStory">
			<%-- <a href="<c:url value="/story/show?id=${story.compoundId}"/>" class="resultTitle">  --%>
			<h2 class="resultTitle">
				<a href="${story.url}">
					<c:out value="${story.title}"/>
				</a>
				<c:if test="${userSession.valid}">
					<c:set var="bookmarkId" value="${userSession.user.bookmarksMap[story.id].id}"/>
					<img src="<c:choose><c:when test="${!empty(bookmarkId)}"><c:url value="/images/fav.png"/></c:when>
								<c:otherwise><c:url value="/images/fav_gray.png"/></c:otherwise></c:choose>" 
						alt='<fmt:message key="bookmark"/>' 
						onclick="bookmark('${story.id}', this); return false;"
						class="bookmark"/>
					<input type="hidden" id="bookmark-story-${story.id}" value="${bookmarkId}"/>
				</c:if>
			</h2>
			<p class="resultDescription">
				<c:out value="${story.description}"/>
			</p>
			<p class="resultAddedDate">
				<fmt:message key="added"/>: 
				<fmt:formatDate value="${story.created}" pattern="MMM dd, yyyy"/>
				
				<c:if test="${!empty story.createdBy.username}">
					<fmt:message key="by"/> <c:out value="${story.createdBy.username}"/>
				</c:if>
				
				&nbsp;&nbsp;
				
				<c:if test="${userSession.valid && !empty story.createdBy && userSession.user.email eq story.createdBy.email}">
					<a href='<c:url value="/story/create"/>?storyId=${story.id}'><fmt:message key="edit"/></a>
					<a href='#' onclick="confirmDelete('${story.id}')"><fmt:message key="delete"/></a>
				</c:if>

				<c:if test="${userSession.valid}">
					<a onclick="flagAsSpam(${story.id}, this)" class="flagAsSpam"><fmt:message key="flag_as_spam"/></a>
				</c:if>
			</p>
		</div>
	</li>
	</c:forEach>
