<%@ include file="/WEB-INF/jsp/includes.jsp" %>

<%@ include file="/WEB-INF/jsp/header.jsp" %>

<div id="main">

<h1><fmt:message key="admin"/></h1>

<div style="padding-left:15px; padding-bottom:40px;">
	<h3>Users (${fn:length(users)})</h3>
	
	<table id="usersTable" cellpadding="0" cellspacing="0" class="admin tablesorter">
		<thead>
			<tr>
				<th><fmt:message key="id"/></th>
				<th><fmt:message key="username"/></th>
				<th><fmt:message key="email"/></th>
				<th><fmt:message key="active"/></th>
				<th><fmt:message key="type"/></th>
				<th><fmt:message key="votes_up"/></th>
				<th><fmt:message key="votes_down"/></th>
				<th><fmt:message key="stories"/></th>
				<th><fmt:message key="bookmarks"/></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="user" items="${users}" varStatus="status">
			<tr <c:choose>
					<c:when test="${status.index%2!=0}">class="odd"</c:when>
					<c:otherwise>class="even"</c:otherwise>
				</c:choose> id="userRow${user.id}">
				<td class="center"><c:out value="${user.id}"/></td>
				<td><c:out value="${user.username}"/></td>
				<td><c:out value="${user.email}"/></td>
				<td><c:out value="${user.active}"/></td>
				<td class="center"><c:out value="${user.type}"/></td>
				<td class="center"><c:out value="${fn:length(user.votesUp)}"/></td>
				<td class="center"><c:out value="${fn:length(user.votesDown)}"/></td>
				<td class="center"><c:out value="${fn:length(user.stories)}"/></td>
				<td class="center"><c:out value="${fn:length(user.bookmarks)}"/></td>
			</tr>
			</c:forEach>
		</tbody>
	</table>
	
	
	<h3>Stories (${fn:length(stories)})</h3>
	<table id="storiesTable" cellpadding="0" cellspacing="0" class="admin tablesorter">
		<thead>
			<tr>
				<th><fmt:message key="id"/></th>
				<th><fmt:message key="title"/></th>
				<th><fmt:message key="description"/></th>
				<th><fmt:message key="up"/></th>
				<th><fmt:message key="down"/></th>
				<th><fmt:message key="spam"/></th>
				<th><fmt:message key="added"/></th>
				<th><fmt:message key="user"/></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="story" items="${stories}" varStatus="status">
			<tr <c:choose>
					<c:when test="${status.index%2!=0}">class="odd"</c:when>
					<c:otherwise>class="even"</c:otherwise>
				</c:choose>>
				<td class="center"><c:out value="${story.id}"/></td>
				<td><a href="${story.url}" title="${story.title}"><c:out value="${fn:substring(story.title, 0, 30)}"/></a></td>
				<td><div title="${story.description}"><c:out value="${fn:substring(story.description, 0, 20)}"/></div></td>
				<td class="center"><c:out value="${story.voteUp}"/></td>
				<td class="center"><c:out value="${story.voteDown}"/></td>
				<td class="center"><c:out value="${story.spamFlags}"/></td>
				<td class="center"><fmt:formatDate value="${story.created}" pattern="MMM dd, yyyy"/></td>
				<td class="center"><a href="#" onclick="return false;" onmouseover="highlightUser(${story.createdBy.id});" onmouseout="unhighlightUser(${story.createdBy.id});"><c:out value="${story.createdBy.id}"/></a></td>
			</tr>
			</c:forEach>
		</tbody>
	</table>
	
	
	<h3>Execute Query (selects only)</h3>
	<textarea id="query" 
		cols="70" rows="5" 
		style="font-family:Courier; font-size:13px; color:#000000;"
		onkeyup="executeIfCtrlEnter(event, 'executeQuery()');" 
		spellcheck="false"></textarea>
	<br/>
	<input type="button" class="button" value="Go" onclick="executeQuery();"/>
	
	<p style="font-family:Courier; font-size:11px; font-style:italic; color:#222222; padding-left:10px;">
		Examples:<br/>
		select id, username from USERS;<br/>
		select id, title, description, created, user_id from STORY where created > '2011-03-15 00:00:00';
	</p>
	
	<div id="executeResultContainer">
	</div>
</div>

</div>

<script type="text/javascript">
	function getUserRow(userId) {
		var elementId = 'userRow'+userId;
		return document.getElementById(elementId);
	}

	function highlightUser(userId) {
		var userRow = getUserRow(userId);
		if (userRow != null) {
			userRow.style.backgroundColor = "#ffe2c6";
		}
	}

	function unhighlightUser(userId) {
		var userRow = getUserRow(userId);
		if (userRow != null) {
			userRow.style.backgroundColor = "";
		}
	}

	function executeQuery() {
		var query = $.trim($("#query").val().replace('\n', ' '));
		if (query == '' || query.charAt(query.length-1) != ';') {
			return;
		}
		var url = '<c:url value="/admin/executeQuery"/>';
		var params = "query=" + query;
		var resultContainerId = "executeResultContainer";
		ajaxGet(url, params, resultContainerId);
	}
</script>

<c:set var="additionalHeadContent" value="/WEB-INF/jsp/admin/tableSort_js.jsp"/>

<%@ include file="/WEB-INF/jsp/footer.jsp" %>