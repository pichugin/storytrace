<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ include file="/WEB-INF/jsp/includes.jsp" %>

<c:choose>
	<c:when test="${!empty(resultSet)}">
		<h4>Result (${fn:length(resultSet.rows)}):</h4>
		<table id="resultSetTable" cellpadding="0" cellspacing="0" class="admin tablesorter">
			<thead>
				<tr>
					<c:forEach var="columnName" items="${resultSet.columnNames}" varStatus="status">
						<th>${columnName}</th>
					</c:forEach>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="row" items="${resultSet.rows}" varStatus="status">
					<tr <c:choose>
							<c:when test="${status.index % 2 != 0}">class="odd"</c:when>
							<c:otherwise>class="even"</c:otherwise>
						</c:choose>>
						<c:choose>
							<c:when test="${fn:length(resultSet.columnNames) > 1}">
								<c:forEach var="columnValue" items="${row}">
									<td><div title="<c:out value="${columnValue}"/>"><c:out value="${fn:substring(columnValue, 0, 20)}"/></div></td>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<td><div title="<c:out value="${row}"/>"><c:out value="${fn:substring(row, 0, 20)}"/></div></td>
							</c:otherwise>
						</c:choose>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		
		<script>
			$("#resultSetTable").tablesorter(); 
		</script>
	</c:when>
	<c:otherwise>
		<p style="color:#ff0000; font-weight:bold;">
			<c:out value="${sqlError}"/>
		</p>
	</c:otherwise>
</c:choose>