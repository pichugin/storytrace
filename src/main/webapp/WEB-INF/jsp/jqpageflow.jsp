<%@ include file="/WEB-INF/jsp/includes.jsp" %>

<script src="<c:url value="/script/jquery/jqpageflow/js/jquery.jqpageflow.js"/>" type="text/javascript"></script> 

<script type="text/javascript">
	$(document).ready(function() {
		$("body").flexiPagination({
			url: flexiPaginationTargetUrl,
				currentPage: 1,
				totalResults: 1000000, //If no more records found in DB we will stop calling server anyway...
				perPage: 10,
				container: "#resultsListId",
				pagerVar : "page",
				loaderImgPath: "../../script/jquery/jqpageflow/images/loader.gif"
		});
	});
</script>



<%@ include file="/WEB-INF/jsp/story/confirmDeletePopup.jsp" %>