<%@ include file="/WEB-INF/jsp/includes.jsp" %>

<div id="changeEmailPopupId"></div>

<script type="text/javascript">
	function changeEmailPopup() {
		
		var popup = $("#changeEmailPopupId").dialog({
			autoOpen : true,
			height : 270,
			width  : 480,
			modal  : true,
			resizable : false,
			draggable : true,
			title: '<fmt:message key="change_email"/>'
		});
		
		$.ajax({
			type: 'GET',
			url: '<c:url value="/user/changeEmail"/>',
			contentType: "application/x-www-form-urlencoded;charset=UTF-8",
			success: function(data) {
				$('#changeEmailPopupId').html(data);
			}
		});
		
	}
</script>