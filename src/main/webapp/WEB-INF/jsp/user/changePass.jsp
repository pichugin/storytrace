<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ include file="/WEB-INF/jsp/includes.jsp" %>

<c:choose>
<c:when test="${passwordChanged}">

	<script type="text/javascript">
		window.location.reload(true);
	</script>
	
</c:when>
<c:otherwise>
	
	<h1><fmt:message key="change_password"/></h1>
	
	<form:form modelAttribute="changePassForm" method="POST" onsubmit="return getConfirmPassState();">
		<table class="form centered">
			<tr>
				<td class="formLabel"><fmt:message key="old_password"/>:</td>
				<td><form:password path="oldPassword" id="oldPassword" cssClass="formInput" onkeyup="executeIfEnter(event, 'changePass()');"/></td>
			</tr>
			<tr>
				<td></td>
				<td><form:errors path="oldPassword" cssClass="errors"/></td>
			</tr>
			
			
			<tr>
				<td class="formLabel"><fmt:message key="new_password"/>:</td>
				<td><form:password path="password" id="firstPassword" cssClass="formInput" onkeyup="executeIfEnter(event, 'changePass()');"/></td>
			</tr>
			<tr>
				<td></td>
				<td><form:errors path="password" cssClass="errors"/></td>
			</tr>
			
			
			<tr>
				<td class="formLabel"><fmt:message key="confirm_new_password"/>:</td>
				<td><input type="password" id="confirmPassword" class="formInput" onfocus="updateConfirmPassState()" onblur="hideConfirmPassState()" onkeyup="updateConfirmPassState(); executeIfEnter(event, 'changePass()');"/></td>
			</tr>
			<tr>
				<td></td>
				<td class="errors" id="confirmPasswordState" style="height:20px;"></td>
			</tr>
		</table>
		<br/>
		
		<input type="button" onclick="changePass()" value="<fmt:message key="change"/>" class="button"/>
		<input type="button" onclick="$('#changePassPopupId').dialog('close');" value='<fmt:message key="cancel"/>' class="button">
	</form:form>
	
	
	<script type="text/javascript">
		document.getElementById('oldPassword').focus();
	
		function changePass() {
			if (getConfirmPassState()) {
				var url = '<c:url value="/user/changePass"/>';
				var params = "oldPassword=" + $("#oldPassword").val() + "&password=" + $("#firstPassword").val();
				var resultContainerId = 'changePassPopupId';
				ajaxPost(url, params, resultContainerId);
			}
		}
	</script>
	<%@ include file="/WEB-INF/jsp/user/confirmPass_js.jsp" %>

</c:otherwise>
</c:choose>
