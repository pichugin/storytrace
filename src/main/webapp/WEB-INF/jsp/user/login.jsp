<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ include file="/WEB-INF/jsp/includes.jsp" %>

<c:choose>
	<c:when test="${userSession.valid}">
		<script type="text/javascript">
			if (typeof(loginCallback) == 'function') {
				loginCallback(function() {
					<%-- window.location.reload(true); // This caused strange infinite loop on older versions of Safari (3.2.*) --%>
					//window.location.href = '<c:url value="/"/>'; <%-- So, after login we always get to the main page :( --%>
					window.location.href = 
						<c:choose>
							<c:when test="${!empty(signonForwardAction)}">
								'<c:url value="/main"/>${signonForwardAction}';
							</c:when>
						<c:otherwise>
							'<c:url value="/story/list"/>';
						</c:otherwise>
					</c:choose>
				});
			} else {
				<%-- window.location.reload(true); // This caused strange infinite loop on older versions of Safari (3.2.*) --%>
				//window.location.href = '<c:url value="/"/>'; <%-- So, after login we always get to the main page :( --%>
				window.location.href = 
					<c:choose>
						<c:when test="${!empty(signonForwardAction)}">
							'<c:url value="/main"/>${signonForwardAction}';
						</c:when>
					<c:otherwise>
						'<c:url value="/story/list"/>';
					</c:otherwise>
				</c:choose>
			}
		</script>
	</c:when>
	<c:otherwise>
		<h1><img src="<c:url value="/images/user.png"/>" alt="User Icon" style="padding-right: 15px;"/><fmt:message key="log_in"/></h1>
		
		<form:form modelAttribute="user" method="POST">
			<table class="form centered">
				<tr>
					<td class="formLabel"><fmt:message key="email_or_username"/>:</td>
					<td><form:input path="email" id="email" cssClass="formInput" maxlength="${maxEmailLength}"/></td>
				</tr>
				
				
				<tr>
					<td class="formLabel"><fmt:message key="password"/>:</td>
					<td><form:password path="password" id="password" cssClass="formInput" maxlength="${maxPasswordLength}" onkeyup="executeIfEnter(event, 'login()')"/></td>
				</tr>
				<tr>
					<td></td>
					<td><form:errors path="email" cssClass="errors"/></td>
				</tr>
			</table>			
			<br/>
			
			<input type="button" onclick="login()" value="<fmt:message key="log_in"/>" class="button"/>
			<input type="button" onclick="$('#authPopupId').dialog('close');" value='<fmt:message key="cancel"/>' class="button">
			<br/>
			
			<a href='<c:url value="/user/forgotPass"/>' class="forgotPasswordLogin"><fmt:message key="forgot_password"/></a>
			<a href='<c:url value="/user/edit"/>' class="forgotPasswordLogin"><fmt:message key="register"/></a>
		</form:form>
		
		<script type="text/javascript">
			document.getElementById('email').focus();

			function login() {
				var url = '<c:url value="/user/login"/>';
				var params = "email=" + $("#email").val() + "&password=" + $("#password").val() + 
						"&signonForwardAction=" + escape("${signonForwardAction}");
				var resultContainerId = 'authPopupId';
				ajaxPost(url, params, resultContainerId);
			}
		</script>
		
	</c:otherwise>
</c:choose>
