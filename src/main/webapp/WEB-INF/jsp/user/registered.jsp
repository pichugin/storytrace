<%@ include file="/WEB-INF/jsp/includes.jsp" %>
<%@ include file="/WEB-INF/jsp/header.jsp" %>

<div id="main">
	<h1><fmt:message key="create_user"/></h1>

	<p style="height:100px;"><fmt:message key="confirmation_email"/></p>
	
	<div style="padding-left:15px; padding-bottom:40px;">
		<input type="button" value='<fmt:message key="ok"/>' class="button" onclick="document.location.href = '<c:url value="/"/>'">
	</div>
</div>

<%@ include file="/WEB-INF/jsp/footer.jsp" %>
 