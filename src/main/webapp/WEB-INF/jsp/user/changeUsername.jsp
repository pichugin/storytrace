<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ include file="/WEB-INF/jsp/includes.jsp" %>

<c:choose>
<c:when test="${usernameChanged}">

	<script type="text/javascript">
		window.location.reload(true);
	</script>
	
</c:when>
<c:otherwise>

	<h1><fmt:message key="change_username"/></h1>
	
	<form:form modelAttribute="changeUsernameForm" method="POST">
		<table class="form centered">
			<tr>
				<td class="formLabel"><fmt:message key="username"/>:</td>
				<td class="formLabel" style="height:30px;">
					<c:out value="${changeUsernameForm.oldUsername}"/>
					<input type="hidden" id="oldUsername" name="oldUsername" value="${changeUsernameForm.oldUsername}"/>
				</td>
			</tr>
			
			
			<tr>
				<td class="formLabel"><fmt:message key="new_username"/>:</td>
				<td><form:input path="username" id="username" cssClass="formInput" onkeyup="executeIfEnter(event, 'changeUsername()');"/></td>
			</tr>
			<tr>
				<td></td>
				<td><form:errors path="username" cssClass="errors"/></td>
			</tr>
			
			
			<tr>
				<td class="formLabel"><fmt:message key="password"/>:</td>
				<td><form:password path="password" cssClass="formInput" onkeyup="executeIfEnter(event, 'changeUsername()');"/></td>
			</tr>
			<tr>
				<td></td>
				<td><form:errors path="password" cssClass="errors"/></td>
			</tr>
		</table>
		<br/>
		
		<input type="button" onclick="changeUsername()" value="<fmt:message key="change"/>" class="button"/>
		<input type="button" onclick="$('#changeUsernamePopupId').dialog('close');" value='<fmt:message key="cancel"/>' class="button">
	</form:form>
	
	<script type="text/javascript">
		document.getElementById('username').focus();
	
		function changeUsername() {
			var url = '<c:url value="/user/changeUsername"/>';
			var params = "oldUsername=" + $("#oldUsername").val() +"&username=" + $("#username").val() + "&password=" + $("#password").val();
			var resultContainerId = 'changeUsernamePopupId';
			ajaxPost(url, params, resultContainerId);
		}
	</script>

</c:otherwise>
</c:choose>
