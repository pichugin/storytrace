<%@ include file="/WEB-INF/jsp/includes.jsp" %>
<%@ include file="/WEB-INF/jsp/header.jsp" %>

<div id="main">

<form:form modelAttribute="user" method="POST">

<h1><fmt:message key="password_reset"/></h1>

<p style="padding:10px; padding-bottom: 0px;">
	<fmt:message key="forgot_pass_message"/>
</p>

<table class="form" style="padding:10px; padding-bottom: 15px;">
	<tr>
		<td class="formLabel"><fmt:message key="email"/>:</td>
		<td><form:input path="email" cssClass="formInput" maxlength="${maxEmailLength}"/></td>
	</tr>
	<tr>
		<td></td>
		<td><form:errors path="email" cssClass="errors"/></td>
	</tr>
</table>
<br/>

<div style="padding-left:15px; padding-bottom:40px;">
	<input type="submit" value="<fmt:message key="submit"/>" class="button">
	<input type="button" value='<fmt:message key="cancel"/>' class="button" onclick="document.location.href = '<c:url value="/story/list"/>'">
</div>

</form:form>

</div>

<%@ include file="/WEB-INF/jsp/footer.jsp" %>
 
 