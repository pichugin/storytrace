<%@ include file="/WEB-INF/jsp/includes.jsp" %>

<c:set var="bodyOnload" value="showAuthPopup();"/>
<%@ include file="/WEB-INF/jsp/header.jsp" %>

<c:choose>
	<c:when test="${userSession.valid}">
		<script type="text/javascript">
			window.location.href = 
				<c:choose>
					<c:when test="${!empty(signonForwardAction)}">
						'<c:url value="/main"/>${signonForwardAction}'
					</c:when>
					<c:otherwise>
						'<c:url value="/story/list"/>'
					</c:otherwise>
				</c:choose>;
		</script>
	</c:when>
	<c:otherwise>
		<div id="main"></div>

		<script type="text/javascript">
			function showAuthPopup() {
				authPopup(true);
			}
		</script>

		<%@ include file="/WEB-INF/jsp/footer.jsp" %>

	</c:otherwise>
</c:choose>
