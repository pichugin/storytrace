<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ include file="/WEB-INF/jsp/includes.jsp" %>

<c:choose>
<c:when test="${emailChanged}">

	<script type="text/javascript">
		window.location.reload(true);
	</script>
	
</c:when>
<c:otherwise>

	<h1><fmt:message key="change_email"/></h1>
	
	<form:form modelAttribute="changeEmailForm" method="POST">
		<table class="form centered">
			<tr>
				<td class="formLabel"><fmt:message key="email"/>:</td>
				<td class="formLabel" style="height:30px;">
					<c:out value="${changeEmailForm.oldEmail}"/>
					<input type="hidden" id="oldEmail" name="oldEmail" value="${changeEmailForm.oldEmail}"/>
				</td>
			</tr>
			
			
			<tr>
				<td class="formLabel"><fmt:message key="new_email"/>:</td>
				<td><form:input path="email" id="email" cssClass="formInput" onkeyup="executeIfEnter(event, 'changeEmail()');"/></td>
			</tr>
			<tr>
				<td></td>
				<td><form:errors path="email" cssClass="errors"/></td>
			</tr>
			
			
			<tr>
				<td class="formLabel"><fmt:message key="password"/>:</td>
				<td><form:password path="password" cssClass="formInput" onkeyup="executeIfEnter(event, 'changeEmail()');"/></td>
			</tr>
			<tr>
				<td></td>
				<td><form:errors path="password" cssClass="errors"/></td>
			</tr>
		</table>
		<br/>
		
		<input type="button" onclick="changeEmail()" value="<fmt:message key="change"/>" class="button"/>
		<input type="button" onclick="$('#changeEmailPopupId').dialog('close');" value='<fmt:message key="cancel"/>' class="button">
	</form:form>
	
	<script type="text/javascript">
		document.getElementById('email').focus();
	
		function changeEmail() {
			var url = '<c:url value="/user/changeEmail"/>';
			var params = "oldEmail=" + $("#oldEmail").val() +"&email=" + $("#email").val() + "&password=" + $("#password").val();
			var resultContainerId = 'changeEmailPopupId';
			ajaxPost(url, params, resultContainerId);
		}
	</script>

</c:otherwise>
</c:choose>
