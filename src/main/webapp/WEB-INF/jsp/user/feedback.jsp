<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ include file="/WEB-INF/jsp/includes.jsp" %>

<c:choose>
<c:when test="${feedbackSent}">

	<script type="text/javascript">
		window.location.reload(true);
	</script>
	
</c:when>
<c:otherwise>
	
	<h1><fmt:message key="feedback"/></h1>
	
	<form:form modelAttribute="feedbackForm" method="POST" onsubmit="return getConfirmPassState();">
		<table class="form centered">
			<tr>
				<td class="formLabel"><fmt:message key="email"/>:</td>
				<td><form:input path="email" id="email" cssClass="formInput" onkeyup="executeIfEnter(event, 'sendFeedback()');"/></td>
			</tr>
			<tr>
				<td></td>
				<td><form:errors path="email" cssClass="errors"/></td>
			</tr>
			
			
			<tr>
				<td class="formLabel"><fmt:message key="subject"/>:</td>
				<td>
					<form:select path="subject" cssClass="formInput" onkeyup="executeIfEnter(event, 'sendFeedback()');">
						<option value="Bug"><fmt:message key="problem"/></option>
						<option value="Enhancement"><fmt:message key="feature_request"/></option>
						<option value="Comment"><fmt:message key="comment"/></option>
					</form:select>
				</td>
			</tr>
			<tr>
				<td></td>
				<td><form:errors path="subject" cssClass="errors"/></td>
			</tr>
			
			
			<tr>
				<td class="formLabel"><fmt:message key="comment"/>:</td>
				<td><form:textarea path="comment" cssClass="formInput formInputWide" rows="8"/></td>
			</tr>
			<tr>
				<td></td>
				<td><form:errors path="comment" cssClass="errors"/></td>
			</tr>
		</table>
		<br/>
		
		<input type="button" onclick="sendFeedback()" value="<fmt:message key="submit"/>" class="button"/>
		<input type="button" onclick="$('#feedbackPopupId').dialog('close');" value='<fmt:message key="cancel"/>' class="button">
	</form:form>
	
	<script type="text/javascript">
		document.getElementById('email').focus();

		var sent = false;
		
		function sendFeedback() {
			if (sent) {
				return;
			}
			sent = true;
			var url = '<c:url value="/user/feedback"/>';
			var params = "email=" + $("#email").val() + "&subject=" + $("#subject").val() + "&comment=" + $("#comment").val() + "&url=" + document.URL;
			var resultContainerId = 'feedbackPopupId';
			ajaxPost(url, params, resultContainerId);
		}
	</script>
	<%@ include file="/WEB-INF/jsp/user/confirmPass_js.jsp" %>

</c:otherwise>
</c:choose>
