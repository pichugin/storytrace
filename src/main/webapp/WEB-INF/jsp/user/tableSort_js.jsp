<%@ include file="/WEB-INF/jsp/includes.jsp" %>

<script src="<c:url value="/script/jquery/tablesorter/jquery.tablesorter.min.js"/>" type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(
		function() {
			$("#storiesTable").tablesorter();
			$("#bookmarksTable").tablesorter();
		}
	); 
</script>