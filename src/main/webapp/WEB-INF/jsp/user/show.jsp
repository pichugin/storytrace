<%@ include file="/WEB-INF/jsp/includes.jsp" %>

<%@ include file="/WEB-INF/jsp/header.jsp" %>

<div id="main">

<%@ include file="/WEB-INF/jsp/changeEmailPopup.jsp" %>
<%@ include file="/WEB-INF/jsp/changeUsernamePopup.jsp" %>
<%@ include file="/WEB-INF/jsp/changePassPopup.jsp" %>

<h1><fmt:message key="user_information"/></h1>

<h3><fmt:message key="account"/></h3>
    
<table cellpadding="0" cellspacing="0" class="userInfo">
	<tr class="odd">
		<td><fmt:message key="email"/>:</td>
		<td>${user.email}</td>
		<td><a href="#" onclick="changeEmailPopup()"><fmt:message key="edit"/></a></td>
	</tr>
	
	<tr>
		<td><fmt:message key="username"/>:</td>
		<td>${user.username}</td>
		<td><a href="#" onclick="changeUsernamePopup()"><fmt:message key="edit"/></a></td>
	</tr>
	
	<tr class="odd">
		<td><fmt:message key="password"/>:</td>
		<td><fmt:message key="password_mask"/></td>
		<td><a href="#" onclick="changePassPopup()"><fmt:message key="edit"/></a></td>
	</tr>
	
	<tr>
		<td><fmt:message key="stories_added"/>:</td>
		<td>${fn:length(user.stories)}</td>
		<td></td>
	</tr>
	
	<tr class="odd">
		<td><fmt:message key="votes"/>:</td>
		<td>
			${fn:length(user.votes)} 
			<span style="text-transform:lowercase; font-style:italic; font-size:10px; padding-left:7px;">
				(${fn:length(user.votesUp)} <fmt:message key="up"/> / ${fn:length(user.votesDown)} <fmt:message key="down"/>)
			</span>
		</td>
		<td></td>
	</tr>
	
	<tr>
		<td><fmt:message key="bookmarks"/>:</td>
		<td>${fn:length(user.bookmarks)}</td>
		<td></td>
	</tr>
</table>



<h3><fmt:message key="my_stories"/></h3>

<c:choose>
<c:when test="${fn:length(user.stories) > 0}">
<table id="storiesTable" cellpadding="0" cellspacing="0" class="admin tablesorter" style="margin-left:10px; margin-bottom:30px;">
	<thead>
		<tr>
			<th><fmt:message key="title"/></th>
			<th><fmt:message key="description"/></th>
			<th><fmt:message key="up"/></th>
			<th><fmt:message key="down"/></th>
			<th><fmt:message key="added"/></th>
			<th class="noHover"></th>
			<th class="noHover"></th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="story" items="${user.stories}" varStatus="status">
		<tr <c:choose>
				<c:when test="${status.index%2!=0}">class="odd"</c:when>
				<c:otherwise>class="even"</c:otherwise>
			</c:choose>>
			<td><a href="${story.url}" title="${story.title}"><c:out value="${fn:substring(story.title, 0, 35)}"/></a></td>
			<td><div title="${story.description}"><c:out value="${fn:substring(story.description, 0, 30)}"/></div></td>
			<td class="center"><c:out value="${story.voteUp}"/></td>
			<td class="center"><c:out value="${story.voteDown}"/></td>
			<td class="center"><fmt:formatDate value="${story.created}" pattern="MMM dd, yyyy"/></td>
			<td style="padding-left:30px; padding-right:10px;">
				<a href='<c:url value="/story/create"/>?storyId=${story.id}'><fmt:message key="edit"/></a>
			</td>
			<td style="padding-left:10px; padding-right:30px;">
				<a href='#' onclick="confirmDelete('${story.id}')"><fmt:message key="delete"/></a>
			</td>
		</tr>
		</c:forEach>
	</tbody>
</table>
</c:when>
<c:otherwise>
	<p style="padding-left:15px; padding-bottom:40px;">
		<fmt:message key="no_stories_added_message"/> 
		<a href='<c:url value="/story/create"/>'><fmt:message key="click_here"/></a>.
	</p>
</c:otherwise>
</c:choose>



<h3><fmt:message key="my_bookmarks"/></h3>

<c:choose>
<c:when test="${fn:length(user.bookmarks) > 0}">
<table id="bookmarksTable" cellpadding="0" cellspacing="0" class="admin tablesorter" style="margin-left:10px; margin-bottom:40px;">
	<thead>
		<tr>
			<th><fmt:message key="title"/></th>
			<th><fmt:message key="description"/></th>
			<th><fmt:message key="added"/></th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="bookmark" items="${user.bookmarks}" varStatus="status">
		<tr <c:choose>
				<c:when test="${status.index%2!=0}">class="odd"</c:when>
				<c:otherwise>class="even"</c:otherwise>
			</c:choose>>
			<td style="width:200px;"><a href="${bookmark.story.url}" title="${bookmark.story.title}"><c:out value="${bookmark.story.title}"/></a></td>
			<td style="width:400px;"><c:out value="${bookmark.story.description}"/></td>
			<td class="center"><fmt:formatDate value="${bookmark.story.created}" pattern="MMM dd, yyyy"/></td>
		</tr>
		</c:forEach>
	</tbody>
</table>
</c:when>
<c:otherwise>
	<p style="padding-left:15px; padding-bottom:40px;">
		<fmt:message key="no_stories_bookmarked_message"/> 
	</p>
</c:otherwise>
</c:choose>

</div>


<c:set var="additionalHeadContent" value="/WEB-INF/jsp/user/show_js.jsp"/>
<%@ include file="/WEB-INF/jsp/footer.jsp" %>
