<%@ include file="/WEB-INF/jsp/includes.jsp" %>
<%@ include file="/WEB-INF/jsp/header.jsp" %>

<c:choose>
	<c:when test="${userSession.valid}">
		<script type="text/javascript">
			window.location.href = '<c:url value="/story/list"/>';
		</script>
	</c:when>
	<c:otherwise>
		<div id="main">
		
		<h1><fmt:message key="create_user"/></h1>
		
		<form:form modelAttribute="registrationForm" method="POST" onsubmit="return getConfirmPassState();">

			<div class="registrationParagraph">
				<h3><fmt:message key="welcome_message"/></h3>
				<p><fmt:message key="registration_message"/></p>
			</div>

			<table class="form" style="padding:10px; padding-bottom: 15px;">
				<tr>
					<td class="formLabel"><fmt:message key="email"/>:</td>
					<td><form:input path="newUser.email" cssClass="formInput" maxlength="${maxEmailLength}"/></td>
				</tr>
				<tr>
					<td></td>
					<td><form:errors path="newUser.email" cssClass="errors"/></td>
				</tr>
				
				
				<tr>
					<td class="formLabel"><fmt:message key="username"/>:</td>
					<td><form:input path="newUser.username" cssClass="formInput" maxlength="${maxUsernameLength}"/></td>
				</tr>
				<tr>
					<td></td>
					<td><form:errors path="newUser.username" cssClass="errors"/></td>
				</tr>
			
			
				<tr>
					<td class="formLabel"><fmt:message key="password"/>:</td>
					<td><form:password id="firstPassword" path="newUser.password" cssClass="formInput" maxlength="${maxPasswordLength}"/></td>
				</tr>
				<tr>
					<td></td>
					<td><form:errors path="newUser.password" cssClass="errors"/></td>
				</tr>
							
			
				<tr>
					<td class="formLabel"><fmt:message key="confirm_password"/>:</td>
					<td><input type="password" id="confirmPassword" class="formInput" maxlength="${maxPasswordLength}" onfocus="updateConfirmPassState()" onblur="hideConfirmPassState()" onkeyup="updateConfirmPassState()"/></td>
				</tr>
				<tr>
					<td></td>
					<td class="errors" style="height:20px;" id="confirmPasswordState"></td>
				</tr>
				
				
				<tr>
					<td></td>
					<td id="captchaContainerId" style="height:120px;"><img src="<c:url value="/captcha/generateImage"/>"/></td>
				</tr>
				<tr>
					<td></td>
					<td style="text-align:center;"><a href="#" onclick="regenerateCaptcha(); return false;"><fmt:message key="change"/></a></td>
				</tr>
				<tr>
					<td><fmt:message key="enter_above_text"/>:</td>
					<td><input type="text" id="captcha" name="captcha" class="formInput" maxlength="50" /></td>
				</tr>
				<tr>
					<td></td>
					<td><form:errors path="captcha" cssClass="errors"/></td>
				</tr>
			</table>
			
			<br/>
			
			<div style="padding-left:15px; padding-bottom:40px;">
				<input type="submit" value="<fmt:message key="create"/>" class="button">
				<input type="button" value='<fmt:message key="cancel"/>' class="button" onclick="document.location.href = '<c:url value="/"/>'">
			</div>
		</form:form>
		
		</div>
		
		
		<script type="text/javascript">
			//Fake param that makes all requests seem unique, s.t. the controller is hit every time.
			var requestId = 1;
		
			function regenerateCaptcha() {
				var myImage = new Image();
				myImage.src = '<c:url value="/captcha/generateImage"/>?requestId=' + requestId;
				requestId += 1;
				$("#captchaContainerId").html(myImage);
			}
		</script>
	</c:otherwise>
</c:choose>

<%@ include file="/WEB-INF/jsp/user/confirmPass_js.jsp" %>
<%@ include file="/WEB-INF/jsp/footer.jsp" %>
