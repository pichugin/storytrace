<script type="text/javascript">
var confPassValid = true;

function hideConfirmPassState() {
	document.getElementById('confirmPasswordState').innerHTML = '';
}

function updateConfirmPassState() {
	confPassValid = (document.getElementById('firstPassword').value == 
					document.getElementById('confirmPassword').value);
	var confPassDiv = document.getElementById('confirmPasswordState');
	var msg = '';
	if (confPassValid) {
		confPassDiv.className = "";
		confPassDiv.innerHTML = '<fmt:message key="confirm_password_pass"/>';
	} else {
		confPassDiv.className = "errors";
		confPassDiv.innerHTML = '<fmt:message key="confirm_password_fail"/>';
	}
}

function getConfirmPassState() {
	updateConfirmPassState();
	return confPassValid;
}
</script>