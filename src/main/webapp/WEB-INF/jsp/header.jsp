<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%@ include file="/WEB-INF/jsp/includes.jsp" %>

<?xml version="1.0"?>
<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>

	<%-- Embedded Firebug for debugging without having to install FB. Works on IE too! --%>
	<%-- 
	<script type="text/javascript" src="https://getfirebug.com/firebug-lite.js"></script>
	--%>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta name="description" content="<fmt:message key="site_description"/>"/>
	<meta name="keywords" content="<fmt:message key="site_keywords"/>"/>
	<title><fmt:message key="site_title"/></title>
	
	<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/themes/smoothness/jquery-ui.css" type="text/css"/> 
	<style>
		/* Override default jquery styling */
		.ui-dialog .ui-dialog-content { 
			background: #fafafa;
		}
		
		.ui-widget-overlay {
			background: #cccccc url(<c:url value="/script/jquery/ui/images/ui-bg_diagonals-thick_20_666666_40x40.png"/>) 50% 50% repeat; 
			opacity: .35;
			filter: Alpha(Opacity=35);
		}
	</style>
	<%--<link rel="stylesheet" href="<c:url value="/script/jquery/ui/jquery.ui.css"/>" type="text/css"/>--%> 
	<link rel="stylesheet" href="<c:url value="/styles/main.css"/>" type="text/css"/>
	<link rel="stylesheet" href="<c:url value="/styles/new.css"/>" type="text/css" id="cssDeclaration"/>
	<script>
		if(screen.width <= 800) {
			document.getElementById('cssDeclaration').href = '<c:url value="/styles/new_mobile.css"/>';
		}
	</script>

<link rel="shortcut icon" href="<c:url value="/images/storytrace.ico"/>" />
	
	<script src="<c:url value="/script/common.js"/>" type="text/javascript"></script>
	
</head>

<body onload="<c:out value="${bodyOnload}"/>" onunload="<c:out value="${bodyOnunload}"/>">




<div class="feedbackTab" onclick="feedbackPopup()"></div>

<div id="header">
	<div class="headerContent">
	
		<ul class="loginPanel">
			<c:choose>
				<c:when test="${userSession.valid}">
					<li><a href="<c:url value="/user/show"/>"><fmt:message key="my_profile"/></a></li>
					<li><a href="<c:url value="/user/logout"/>"><fmt:message key="log_out"/></a></li>
				</c:when>
				<c:otherwise>
					<li><a href="#" onclick="authPopup(false)"><fmt:message key="log_in"/></a></li>
					<li><a href="<c:url value="/user/edit"/>"><fmt:message key="register"/></a></li>
				</c:otherwise>
			</c:choose>
		</ul>
	
		<ul class="menu">
			<li>
				<a href="<c:url value="/story/list"/>" class="menuItem"><fmt:message key="home"/></a>
			</li>
			<li>|</li>
			<li>
				<a href="<c:url value="/about/show"/>"><fmt:message key="about"/></a>
			</li>
			<c:if test="${userSession.admin}">
			<li>|</li>
			<li>
				<a href="<c:url value="/admin/show"/>"><fmt:message key="admin"/></a>
			</li>
			</c:if>
		</ul>
		
	</div>
</div>