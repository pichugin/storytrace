<%@ include file="/WEB-INF/jsp/includes.jsp" %>

<div id="feedbackPopupId"></div>

<script type="text/javascript">
	function feedbackPopup() {
		
		var popup = $("#feedbackPopupId").dialog({
			autoOpen : true,
			height : 410,
			width  : 500,
			modal  : true,
			resizable : false,
			draggable : true,
			title: '<fmt:message key="feedback"/>'
		});
		
		$.ajax({
			type: 'GET',
			url: '<c:url value="/user/feedback"/>',
			contentType: "application/x-www-form-urlencoded;charset=UTF-8",
			success: function(data) {
				$('#feedbackPopupId').html(data);
			}
		});
		
	}
</script>