
	
	<div class="footer">
		<fmt:message key="copyright"/>
	</div>

	<script src="http://code.jquery.com/jquery-1.4.2.min.js" type="text/javascript"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js" type="text/javascript"></script> 
	<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
	<script type="text/javascript">
		(function() {
		var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
		s.type = 'text/javascript';
		s.async = true;
		s.src = 'http://widgets.digg.com/buttons.js';
		s1.parentNode.insertBefore(s, s1);
		})();
	</script>
	
	<%--<script src="<c:url value="/script/jquery/jquery-1.4.2.min.js"/>" type="text/javascript"></script> --%>
	<%--<script src="<c:url value="/script/jquery/ui/jquery.ui.min.js"/>" type="text/javascript"></script>--%>
	
	<%@ include file="/WEB-INF/jsp/authPopup.jsp" %>
	<%@ include file="/WEB-INF/jsp/feedbackPopup.jsp" %>
	
	<%-- If there are more scripts or other resources to include in the HEAD tag use the following variable: 
		 <c:set var="additionalHeadContent" value="/WEB-INF/jsp/place/head.jsp"/> --%>
	<c:if test="${!empty additionalHeadContent}">
		<jsp:include page="${additionalHeadContent}"></jsp:include>
	</c:if>
	 
	<%-- Google Analytics goes here: --%>
	<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push( [ '_setAccount', 'UA-15264078-3' ]);
		_gaq.push( [ '_trackPageview' ]);
	
		(function() {
			var ga = document.createElement('script');
			ga.type = 'text/javascript';
			ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl'
					: 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(ga, s);
		})();
	</script>

</body>
</html>
