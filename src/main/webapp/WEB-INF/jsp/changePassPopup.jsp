<%@ include file="/WEB-INF/jsp/includes.jsp" %>

<div id="changePassPopupId"></div>

<script type="text/javascript">
	function changePassPopup() {
		
		var popup = $("#changePassPopupId").dialog({
			autoOpen : true,
			height : 300,
			width  : 480,
			modal  : true,
			resizable : false,
			draggable : true,
			title: '<fmt:message key="change_password"/>'
		});
		
		$.ajax({
			type: 'GET',
			url: '<c:url value="/user/changePass"/>',
			contentType: "application/x-www-form-urlencoded;charset=UTF-8",
			success: function(data) {
				$('#changePassPopupId').html(data);
			}
		});
		
	}
</script>