package com.sagg.util;

import java.io.ByteArrayInputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;


public class JAXBUtil <T> {

	@SuppressWarnings("unchecked")
	public T parse(String xml, Class<?> clazz) throws Exception {
		JAXBContext jc = JAXBContext.newInstance(clazz);
		Unmarshaller u = jc.createUnmarshaller();
		T address = (T) u.unmarshal(new ByteArrayInputStream(xml.getBytes("UTF-8")));
		System.out.println(address.toString());
		return address;
	}
	
}
