package com.sagg.util;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

import com.sagg.domain.DomainObject;

public class DomainObjectUtil {

	public static void removeFromCollection(Collection<? extends DomainObject> col, Integer id) {
		for (Iterator<? extends DomainObject> it = col.iterator(); it.hasNext(); ) {
			if (it.next().getId().equals(id)) {
				it.remove();
				return;
			}
		}
	}
	
	public static boolean contains(Set<? extends DomainObject> set, DomainObject domainObject) {
		if (domainObject == null || domainObject.getId() == null) {
			return false;
		}
		for (Iterator<? extends DomainObject> it = set.iterator(); it.hasNext(); ) {
			DomainObject c = it.next();
			if (c.getId().equals(domainObject.getId())) {
				return true;
			}
		}
		return false;
	}

}
