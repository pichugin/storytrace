package com.sagg.util;

public class EmailUtil {

	private static final String EMAIL_REGEX = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$"; 
	
	public static boolean isValidEmail(String email) {
		if (StringUtil.isEmpty(email)) {
			return false;
		}
		if (!email.matches(EMAIL_REGEX)) {
			return false;
		}
		return true;
	}
	
}
