package com.sagg.util;

public class Globals {
	
	public static final int MAX_USER_EMAIL_LENGTH = 100;
	public static final int MAX_USER_USERNAME_LENGTH = 100;
	public static final int MAX_USER_PASSWORD_LENGTH = 100;

	public static final int MAX_STORY_URL_LENGTH = 250;
	public static final int MAX_STORY_TITLE_LENGTH = 60;
	public static final int MAX_STORY_DESCRIPTION_LENGTH = 800;
	public static final int MAX_STORY_THUMBNAIL_URL_LENGTH = 250;
	
	public static final int MAX_SEARCH_QUERY_LENGTH = 150;
	
	public static final int MAX_FEEDBACK_COMMENT_LENGTH = 800;
	
	public static final String FEEDBACK_EMAIL = "info@storytrace.com";
	
	public static final int USER_TYPE_ADMIN = 1;
	public static final int USER_TYPE_REGULAR = 9; //The lower the value, the more restrictive access is.
	
	public static final Integer VOTE_UP = 1;
	public static final Integer VOTE_DOWN = -1;
	
}
