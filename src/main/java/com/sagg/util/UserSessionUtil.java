package com.sagg.util;

import javax.servlet.http.HttpSession;

import com.sagg.domain.User;
import com.sagg.service.UserService;
import com.sagg.web.UserSession;

public class UserSessionUtil {
	
	public static User refreshUser(HttpSession session, UserService userService) throws Exception {
		UserSession userSession = (UserSession) session.getAttribute(UserSession.ID);
		User user = null;
		if (userSession != null && userSession.isValid()) {
			user = userService.getUser(userSession.getUser().getId());
			userSession.setUser(user);
		}
		return user;
	}

}
