package com.sagg.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class StringUtil {

	public static boolean isEmpty(String s) {
		return s == null || "".equals(s.trim());
	}

	public static Collection<String> split(String s, String from, String to, String delimiter) {
		List<String> result = new ArrayList<String>(); 
		int fromIndex = s.indexOf(from);
		fromIndex = fromIndex < 0 ? 0 : fromIndex + from.length();
		int toIndex = s.indexOf(to, fromIndex);
		s = s.substring(fromIndex, toIndex < 0 ? s.length() : toIndex);
		for (String element : s.split(delimiter)) {
			result.add(element.trim());
		}
		return result;
	}

}
