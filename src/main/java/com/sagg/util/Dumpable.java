package com.sagg.util;

/** 
 * Classes that implement this interface can call 
 * {@link ObjectDumber} from their toString() method.  
 * */
public interface Dumpable {
}
