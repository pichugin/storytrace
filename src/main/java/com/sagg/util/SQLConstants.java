package com.sagg.util;

public class SQLConstants {

	public static final String AND = " AND ";
	public static final String OR = " OR ";
	public static final String TRUE = "TRUE";
	public static final String FALSE = "FALSE";
	public static final String IS_NULL = " IS NULL ";
	
}
