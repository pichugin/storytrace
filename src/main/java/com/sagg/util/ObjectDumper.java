package com.sagg.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ObjectDumper {
	
	private List<Object> objectRegistry = new ArrayList<Object>();
	
	public String toString(Dumpable obj) {
		StringBuilder s = new StringBuilder(obj.getClass().getName() + "@" + Integer.toHexString(obj.hashCode()) + ":[\n");
		if (!objectRegistry.contains(obj.hashCode())) {
			objectRegistry.add(obj.hashCode());
			toStringUp(obj.getClass(), obj, s);
		} else {
			s.append("\t...\n");
		}
		return s.append("]").toString();
	}
	
	private void toStringUp(Class<?> clazz, Object obj, StringBuilder s) {
		if (clazz.equals(Object.class)) {
			return;
		}
		Field[] fields = clazz.getDeclaredFields();
		for (Field field : fields) {
			try {
				String fieldValue = null;
				try {
					String fieldName = field.getName();
					String getterName = "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
					Method method = clazz.getMethod(getterName);
					Object object = method.invoke(obj);
					if (object instanceof Dumpable) {
						fieldValue = toString((Dumpable) object);
					} else if (object instanceof Collection<?>) {
						Collection<?> col = (Collection<?>) object;
						fieldValue = "...size:" + col.size() + "...";
						//TODO: print out the collection's elements (recursively check for collection) - this may take a lot of space though!
					} else {
						fieldValue = "" + object;
					}
				} catch (InvocationTargetException e) {
					//e.printStackTrace();
				}
				s.append("\t").append((field.getName() + " = " + fieldValue).replaceAll("\n", "\n\t")).append("\n");
			} catch (NoSuchMethodException e) {
				// If there is no getter defined for this field, just skip it.
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		toStringUp(clazz.getSuperclass(), obj, s);
	}

}
