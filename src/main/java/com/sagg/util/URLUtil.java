package com.sagg.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class URLUtil {
	
	public static String normalizeUrl(String url) {
		if (url != null && !url.startsWith("http://")) {
			url = "http://" + url;
		}
		return url;
	}
	
	/**
	 * Returns true if the URL is valid and can be connected to.
	 */
	public static boolean isValidUrl(String urlStr) {
		try {
			URL url = new URL(urlStr);
			URLConnection conn = url.openConnection(); //(HttpURLConnection) url.openConnection();
			conn.connect();
			
			// If the URL is syntactically correct but is crap by meaning we'll successfully 
			// get up to this point, but the following will throw an exception:
			conn.getInputStream().close();
			
			// We can also make sure that:
			// "200".equals(conn.getResponseCode());
			// "OK".equals(conn.getResponseMessage());
			
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * Send a get request to the URL and return the title of the page.
	 * @throws Exception 
	 */
	public static String getTitle(URL url) throws Exception {
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		connection.setDoOutput(true);
		connection.setReadTimeout(10000);
		connection.connect();

		BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String line = null;
		while ((line = rd.readLine()) != null) {
			if (line.contains("<title>") && line.contains("</title>")) {
				return extractTitle(line);
			}
		}
		return "";
	}
	
	private static String extractTitle(String str) {
		str = str.substring(str.indexOf("<title>") + "<title>".length());
		str = str.substring(0, str.indexOf("</title>"));
		return str;
	}
	
}
