package com.sagg.web.form;

import org.springframework.util.StringUtils;

import com.sagg.domain.User;
import com.sagg.exception.ValidationFailedException;
import com.sagg.util.EmailUtil;
import com.sagg.util.Globals;

public class ChangeEmailForm {

	private User user = new User();
	private String email;
	
	public String getOldEmail() {
		return user.getEmail();
	}
	public void setOldEmail(String email) {
		this.user.setEmail(email);
	}
	
	public String getPassword() {
		return user.getPassword();
	}
	public void setPassword(String pass) {
		this.user.setPassword(pass);
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public void validate() throws ValidationFailedException {
		if (!StringUtils.hasLength(getOldEmail())) {
			throw new ValidationFailedException("oldEmail", "err.old_email_empty", "*** The old email can not be empty. ***");
		}
		if (!EmailUtil.isValidEmail(this.email)) {
			throw new ValidationFailedException("email", "err.email_invalid", "*** Invalid email. ***");
		}
		if (getOldEmail().equals(this.email)) {
			throw new ValidationFailedException("email", "err.same_old_and_new_email", "*** The old and new email are the same. ***");
		}
		if (this.email.length() > Globals.MAX_USER_EMAIL_LENGTH) {
			throw new ValidationFailedException("email", "err.email_too_long", new String[]{""+Globals.MAX_USER_EMAIL_LENGTH}, "*** Email is too long. ***");
		}
	}
	
}
