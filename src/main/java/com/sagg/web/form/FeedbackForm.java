package com.sagg.web.form;

import com.sagg.exception.ValidationFailedException;
import com.sagg.util.Globals;
import com.sagg.util.EmailUtil;
import com.sagg.util.StringUtil;

public class FeedbackForm {

	private String email;
	private String subject;
	private String comment;
	private String url; //the site url when feedback was sent
	
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUrl() {
		return url;
	}
	
	
	public void validate() throws ValidationFailedException {
		if (!StringUtil.isEmpty(getEmail())) {
			if (!EmailUtil.isValidEmail(getEmail())) {
				throw new ValidationFailedException("email", "err.email_invalid", "*** Invalid email. ***");
			}
			if (getEmail().length() > Globals.MAX_USER_EMAIL_LENGTH) {
				throw new ValidationFailedException("email", "err.email_too_long", new String[]{""+Globals.MAX_USER_EMAIL_LENGTH}, "*** Email is too long. ***");
			}
		}
		if (StringUtil.isEmpty(getComment())) {
			throw new ValidationFailedException("comment", "err.comment_empty", "*** Comment can not be empty. ***");
		}
		if (getComment().length() > Globals.MAX_FEEDBACK_COMMENT_LENGTH) {
			throw new ValidationFailedException("comment", "err.comment_too_long", new String[]{""+Globals.MAX_FEEDBACK_COMMENT_LENGTH}, "*** Comment is too long. ***");
		}
	}
	
}
