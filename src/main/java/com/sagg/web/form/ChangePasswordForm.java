package com.sagg.web.form;

import com.sagg.domain.User;
import com.sagg.exception.ValidationFailedException;
import com.sagg.util.Globals;
import com.sagg.util.StringUtil;

public class ChangePasswordForm {

	private User user = new User();
	private String password;
	
	
	public String getEmail() {
		return user.getEmail();
	}
	public void setEmail(String email) {
		user.setEmail(email);
	}
	
	public String getOldPassword() {
		return user.getPassword();
	}
	public void setOldPassword(String password) {
		user.setPassword(password);
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String newPassword) {
		this.password = newPassword;
	}
	
	
	public void validate() throws ValidationFailedException {
		if (getPassword().equals(getOldPassword())) {
			throw new ValidationFailedException("password", "err.same_old_and_new_password", "*** The old and new password are the same. ***");
		}
		if (StringUtil.isEmpty(getPassword())) {
			throw new ValidationFailedException("password", "err.password_empty", "*** Password can not be empty. ***");
		}
		if (getPassword().length() > Globals.MAX_USER_PASSWORD_LENGTH) {
			throw new ValidationFailedException("password", "err.password_too_long", new String[]{""+Globals.MAX_USER_PASSWORD_LENGTH}, "*** Password is too long. ***");
		}
	}
	
}
