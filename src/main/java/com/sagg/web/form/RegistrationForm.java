package com.sagg.web.form;

import com.sagg.domain.User;
import com.sagg.exception.ValidationFailedException;
import com.sagg.util.Globals;
import com.sagg.util.EmailUtil;
import com.sagg.util.StringUtil;

public class RegistrationForm {

	private User newUser;
	private String captcha;

	
	public RegistrationForm() {
		this.newUser = new User();
	}
	
	public RegistrationForm(User user) {
		this.newUser = user;
	}
	
	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}
	
	public String getCaptcha() {
		return captcha;
	}

	public User getNewUser() {
		return newUser;
	}

	public void setNewUser(User user) {
		this.newUser = user;
	}
	

	public void validate() throws ValidationFailedException {
		//TODO: Find a way to reuse the user's validation so that we don't have to repeat these:
		if (!EmailUtil.isValidEmail(getNewUser().getEmail())) {
			throw new ValidationFailedException("newUser.email", "err.email_invalid", "*** Invalid email. ***");
		}
		if (getNewUser().getEmail().length() > Globals.MAX_USER_EMAIL_LENGTH) {
			throw new ValidationFailedException("newUser.email", "err.email_too_long", new String[]{""+Globals.MAX_USER_EMAIL_LENGTH}, "*** Email is too long. ***");
		}
		if (getNewUser().getUsername().length() > Globals.MAX_USER_USERNAME_LENGTH) {
			throw new ValidationFailedException("newUser.username", "err.username_too_long", new String[]{""+Globals.MAX_USER_USERNAME_LENGTH}, "*** Username is too long. ***");
		}
		if (StringUtil.isEmpty(getNewUser().getPassword())) {
			throw new ValidationFailedException("newUser.password", "err.password_empty", "*** Password can not be empty. ***");
		}
		if (getNewUser().getPassword().length() > Globals.MAX_USER_PASSWORD_LENGTH) {
			throw new ValidationFailedException("newUser.password", "err.password_too_long", new String[]{""+Globals.MAX_USER_PASSWORD_LENGTH}, "*** Password is too long. ***");
		}
		
		//TODO: Validate captcha!!
	}
	
}
