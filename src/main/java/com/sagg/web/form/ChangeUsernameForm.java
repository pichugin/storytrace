package com.sagg.web.form;

import com.sagg.domain.User;
import com.sagg.exception.ValidationFailedException;
import com.sagg.util.Globals;

public class ChangeUsernameForm {

	private User user = new User();
	private String username;
	
	public String getOldUsername() {
		return user.getUsername();
	}
	public void setOldUsername(String name) {
		this.user.setUsername(name);
	}
	
	public String getPassword() {
		return user.getPassword();
	}
	public void setPassword(String pass) {
		this.user.setPassword(pass);
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String name) {
		this.username = name;
	}
	
	public void validate() throws ValidationFailedException {
		if (getOldUsername().equals(this.username)) {
			throw new ValidationFailedException("username", "err.same_old_and_new_username", "*** The old and new usernames are the same. ***");
		}
		if (this.username.length() > Globals.MAX_USER_USERNAME_LENGTH) {
			throw new ValidationFailedException("username", "err.username_too_long", new String[]{""+Globals.MAX_USER_USERNAME_LENGTH}, "*** Username is too long. ***");
		}
	}
	
}
