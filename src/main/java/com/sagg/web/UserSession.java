package com.sagg.web;

import java.io.Serializable;

import com.sagg.domain.User;
import com.sagg.util.Globals;


public class UserSession implements Serializable {

	private static final long serialVersionUID = 1L;
	public static final String ID = "userSession";
	
	private User user;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public boolean isValid() {
		return user != null && user.getId() != null;
	}
	
	public boolean isInvalid() {
		return !isValid();
	}
	
	public boolean isAdmin() {
		return isValid() && Integer.valueOf(Globals.USER_TYPE_ADMIN).equals(user.getType());
	}

}
