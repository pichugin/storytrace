package com.sagg.web.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.octo.captcha.service.CaptchaServiceException;
import com.octo.captcha.service.image.ImageCaptchaService;
import com.sagg.domain.User;
import com.sagg.exception.ValidationFailedException;
import com.sagg.service.UserService;
import com.sagg.util.Globals;
import com.sagg.web.Authorizable;
import com.sagg.web.UserSession;
import com.sagg.web.form.ChangeEmailForm;
import com.sagg.web.form.ChangePasswordForm;
import com.sagg.web.form.ChangeUsernameForm;
import com.sagg.web.form.FeedbackForm;
import com.sagg.web.form.RegistrationForm;

@Controller
@RequestMapping("/user")
public class UserController {
	
	private final UserService userService;
	private final ImageCaptchaService captchaService;

	@Autowired
	public UserController(UserService userService, ImageCaptchaService captchaService) {
		this.userService = userService;
		this.captchaService = captchaService;
	}

	
	
	/* (non-Javadoc)
	 * @see com.sagg.web.controller.UserController#logout(javax.servlet.http.HttpSession)
	 */
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(HttpSession session) {
		session.removeAttribute(UserSession.ID);
		return "redirect:/main/story/list";
	}

	
	
	/* (non-Javadoc)
	 * @see com.sagg.web.controller.UserController#login(java.lang.String, org.springframework.ui.ModelMap)
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(
			@RequestParam(value="signonForwardAction", required=false) String forwardAction,
			ModelMap modelMap) {
		modelMap.addAttribute("signonForwardAction", forwardAction);
		modelMap.addAttribute("maxEmailLength", Globals.MAX_USER_EMAIL_LENGTH);
		modelMap.addAttribute("maxPasswordLength", Globals.MAX_USER_PASSWORD_LENGTH);
		modelMap.addAttribute("user", new User());
		return "user/login";
	}

	/* (non-Javadoc)
	 * @see com.sagg.web.controller.UserController#login(java.lang.String, com.sagg.domain.User, org.springframework.validation.BindingResult, javax.servlet.http.HttpSession, org.springframework.ui.ModelMap)
	 */
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(
			@RequestParam(value="signonForwardAction", required=false) String forwardAction, 
			User user, BindingResult result, HttpSession session /*WebRequest webRequest*/,
			ModelMap modelMap) throws Exception {

		modelMap.addAttribute("signonForwardAction", forwardAction);

		//TODO: hmm... this is error prone. If we follow this logic we shouldn't allow '@' in the username then.
		//      We can make a guess, if the entered string has the email format, TRY to find the user by email,
		//      if not found use it as the username.
		//If user entered username to log in, move it to the proper field:
		if (!user.getEmail().contains("@")) {
			user.setUsername(user.getEmail());
			user.setEmail(null);
		}
		String passEntered = user.getPassword();
		
		user = userService.login(user);
		if (user == null || !user.getPassword().equals(passEntered)) {
			result.rejectValue("email", "err.invalid_user_or_password", "*** Invalid User or Password ***");
			return "user/login";
		}
		if (user.getActive() == null || !user.getActive()) {
			result.rejectValue("email", "err.user_inactive", "*** User has not been activated ***");
			return "user/login";
		}
		addUserToSession(user, session);

		return "user/login";
	}
	
	
	/* (non-Javadoc)
	 * @see com.sagg.web.controller.UserController#loginex(java.lang.String, org.springframework.ui.ModelMap)
	 */
	@RequestMapping(value = "/loginex", method = RequestMethod.GET)
	public String loginex(
			@RequestParam(value="signonForwardAction", required=false) String forwardAction,
			ModelMap modelMap) throws Exception {

		modelMap.addAttribute("signonForwardAction", forwardAction);
		return "user/loginex";
	}

	
	/* (non-Javadoc)
	 * @see com.sagg.web.controller.UserController#show(javax.servlet.http.HttpSession, org.springframework.ui.ModelMap)
	 */
	@Authorizable
	@RequestMapping(value = "/show", method = RequestMethod.GET)
	public String show(HttpSession session, ModelMap modelMap) throws Exception {
		UserSession ses = (UserSession) session.getAttribute(UserSession.ID);
		User user = ses == null ? new User() : userService.getUser((ses).getUser().getId());
		modelMap.addAttribute("user", user);
		return "user/show";
	}

	

	//TODO: Rename to 'register' or 'create'! We don't have the 'edit' functionality for the user.
	/* (non-Javadoc)
	 * @see com.sagg.web.controller.UserController#edit(javax.servlet.http.HttpSession, org.springframework.ui.ModelMap)
	 */
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String edit(HttpSession session, ModelMap modelMap) throws Exception {
		RegistrationForm user = new RegistrationForm(); 
		modelMap.addAttribute("registrationForm", user);
		populateUserMaxLengths(modelMap);
		return "user/edit";
	}
	
	/* (non-Javadoc)
	 * @see com.sagg.web.controller.UserController#edit(com.sagg.web.form.RegistrationForm, org.springframework.validation.BindingResult, javax.servlet.http.HttpSession, org.springframework.ui.ModelMap, java.lang.String)
	 */
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public String edit(
			RegistrationForm registrationForm, 
			BindingResult result, 
			HttpSession session,
			ModelMap modelMap,
			@RequestParam(value="signonForwardAction", required=false) String forwardAction) throws Exception {
		User user = registrationForm.getNewUser();
		populateUserMaxLengths(modelMap);
		
		try {
			registrationForm.validate();
		} catch (ValidationFailedException e) {
			result.rejectValue(e.getFieldName(), e.getErrorKey(), e.getParameters(), e.getMessage());
			return "user/edit";
		}
		
		if (!validateCaptcha(registrationForm.getCaptcha(), result, session.getId(), "err.captcha")) {
			return "user/edit";
		}
		
		if (userService.getUserByEmail(user.getEmail()) != null) {
			result.rejectValue("newUser.email", "err.user_email_already_exists", "*** User with this email already registered ***");
			return "user/edit";
		}
		
		if (userService.getUserByUsername(user.getUsername()) != null) {
			result.rejectValue("newUser.username", "err.user_username_already_exists", "*** User with this username already registered ***");
			return "user/edit";
		}
		
		userService.register(user);
		return "user/registered"; //At this stage the user is not logged in yet. They have to activate the account first.
	}
	
	private void populateUserMaxLengths(ModelMap modelMap) {
		modelMap.addAttribute("maxEmailLength", Globals.MAX_USER_EMAIL_LENGTH);
		modelMap.addAttribute("maxUsernameLength", Globals.MAX_USER_USERNAME_LENGTH);
		modelMap.addAttribute("maxPasswordLength", Globals.MAX_USER_PASSWORD_LENGTH);
	}
	
	
	
	/* (non-Javadoc)
	 * @see com.sagg.web.controller.UserController#activateAccount(java.lang.String, javax.servlet.http.HttpSession)
	 */
	@RequestMapping(value = "/activateAccount", method = RequestMethod.GET)
	public String activateAccount(@RequestParam(required=true) String uuid, 
			/*BindingResult result, */HttpSession session) throws Exception {
		User user = userService.activate(uuid);
		if (user == null || user.getActive() == null || !user.getActive()) {
//			result.reject("activation.failed", "*** Activation Failed! Either the key is invalid or the user has already been activated. ***");
			//TODO: Show an error message We cannot use a BindingResult object during the GET request :(
			return "redirect:/main/story/list";
		}
		addUserToSession(user, session);
		return "user/activated";
	}
	
	
	
	/* (non-Javadoc)
	 * @see com.sagg.web.controller.UserController#forgotPass(javax.servlet.http.HttpSession, org.springframework.ui.ModelMap)
	 */
	@RequestMapping(value = "/forgotPass", method = RequestMethod.GET)
	public String forgotPass(HttpSession session, ModelMap modelMap) throws Exception {
		modelMap.addAttribute("maxEmailLength", Globals.MAX_USER_EMAIL_LENGTH);
		modelMap.addAttribute("user", new User());
		return "user/forgotPass";
	}
	
	/* (non-Javadoc)
	 * @see com.sagg.web.controller.UserController#forgotPass(com.sagg.domain.User, org.springframework.validation.BindingResult, javax.servlet.http.HttpSession, org.springframework.ui.ModelMap)
	 */
	@RequestMapping(value = "/forgotPass", method = RequestMethod.POST)
	public String forgotPass(@ModelAttribute("user") User user, BindingResult result, HttpSession session, ModelMap modelMap) throws Exception {
		user = userService.login(user);
		
		if (user == null) {
			result.rejectValue("email", "err.email_invalid", "*** Invalid email ***");
			return "user/forgotPass";
		}
		
		
		
		/*
		 * TODO: Change password, and send the new pass to the user by email.
		 * 
		 * Problem: We can't change the password at this point because until we get a response to
		 * the email we have no guarantee that this is the owner of the account.
		 * 
		 * Perhaps we need to create a separate table, like the user activate table, that will 
		 * store requests for password changes, and the new generated passwords.
		 * 
		 * For now we will simply email the user their existing password.
		 */
		userService.sendPassword(user.getEmail());
		
		

		return "redirect:/main/story/list";
	}
	
	
	
	/* (non-Javadoc)
	 * @see com.sagg.web.controller.UserController#changePass(javax.servlet.http.HttpSession, org.springframework.ui.ModelMap)
	 */
	@RequestMapping(value = "/changePass", method = RequestMethod.GET)
	public String changePass(HttpSession session, ModelMap modelMap) throws Exception {
		modelMap.addAttribute("changePassForm", new ChangePasswordForm());
		return "user/changePass";
	}
	
	/* (non-Javadoc)
	 * @see com.sagg.web.controller.UserController#changePass(com.sagg.web.form.ChangePasswordForm, org.springframework.validation.BindingResult, javax.servlet.http.HttpSession, org.springframework.ui.ModelMap)
	 */
	@RequestMapping(value = "/changePass", method = RequestMethod.POST)
	public String changePass(@ModelAttribute("changePassForm") ChangePasswordForm changePassForm, BindingResult result, HttpSession session, ModelMap modelMap) throws Exception {
		UserSession ses = (UserSession) session.getAttribute(UserSession.ID);
		User currentUser = userService.getUser((ses).getUser().getId());
		
		if (!currentUser.getPassword().equals(changePassForm.getOldPassword())) {
			result.rejectValue("oldPassword", "err.invalid_old_password", "*** Invalid old password ***");
			return "user/changePass";
		}
		try {
			changePassForm.validate();
		} catch (ValidationFailedException e) {
			result.rejectValue(e.getFieldName(), e.getErrorKey(), e.getParameters(), e.getMessage());
			return "user/changePass";
		}
		
		userService.changePassword(currentUser.getEmail(), changePassForm.getPassword());
		
		modelMap.addAttribute("passwordChanged", true);
		return "user/changePass";
	}
	
	
	
	/* (non-Javadoc)
	 * @see com.sagg.web.controller.UserController#changeEmail(javax.servlet.http.HttpSession, org.springframework.ui.ModelMap)
	 */
	@RequestMapping(value = "/changeEmail", method = RequestMethod.GET)
	public String changeEmail(HttpSession session, ModelMap modelMap) throws Exception {
		UserSession ses = (UserSession) session.getAttribute(UserSession.ID);
		User user = (ses == null) ? new User() : userService.getUser((ses).getUser().getId());
		
		ChangeEmailForm form = new ChangeEmailForm();
		form.setOldEmail(user.getEmail());
		modelMap.addAttribute("changeEmailForm", form);
		return "user/changeEmail";
	}
	
	/* (non-Javadoc)
	 * @see com.sagg.web.controller.UserController#changeEmail(com.sagg.web.form.ChangeEmailForm, org.springframework.validation.BindingResult, javax.servlet.http.HttpSession, org.springframework.ui.ModelMap)
	 */
	@RequestMapping(value = "/changeEmail", method = RequestMethod.POST)
	public String changeEmail(@ModelAttribute("changeEmailForm") ChangeEmailForm changeEmailForm, BindingResult result, HttpSession session, ModelMap modelMap) throws Exception {
		UserSession ses = (UserSession) session.getAttribute(UserSession.ID);
		User currentUser = userService.getUser((ses).getUser().getId());
		
		if (!currentUser.getPassword().equals(changeEmailForm.getPassword())) {
			result.rejectValue("password", "err.invalid_password", "*** Invalid password ***");
			return "user/changeEmail";
		}
		try {
			changeEmailForm.validate();
		} catch (ValidationFailedException e) {
			result.rejectValue(e.getFieldName(), e.getErrorKey(), e.getParameters(), e.getMessage());
			return "user/changeEmail";
		}
		
		User user = userService.changeEmail(currentUser.getEmail(), changeEmailForm.getEmail());
		if (user == null) {
			result.rejectValue("email", "err.user_email_already_exists", null, "*** User with this email already registered ***");
			return "user/changeEmail";
		}
		
		addUserToSession(user, session);
		modelMap.addAttribute("emailChanged", true);
		return "user/changeEmail";
	}
	
	
	
	/* (non-Javadoc)
	 * @see com.sagg.web.controller.UserController#changeUsername(javax.servlet.http.HttpSession, org.springframework.ui.ModelMap)
	 */
	@RequestMapping(value = "/changeUsername", method = RequestMethod.GET)
	public String changeUsername(HttpSession session, ModelMap modelMap) throws Exception {
		UserSession ses = (UserSession) session.getAttribute(UserSession.ID);
		User user = (ses == null) ? new User() : userService.getUser((ses).getUser().getId());
		
		ChangeUsernameForm form = new ChangeUsernameForm();
		form.setOldUsername(user.getUsername());
		modelMap.addAttribute("changeUsernameForm", form);
		return "user/changeUsername";
	}
	
	/* (non-Javadoc)
	 * @see com.sagg.web.controller.UserController#changeUsername(com.sagg.web.form.ChangeUsernameForm, org.springframework.validation.BindingResult, javax.servlet.http.HttpSession, org.springframework.ui.ModelMap)
	 */
	@RequestMapping(value = "/changeUsername", method = RequestMethod.POST)
	public String changeUsername(@ModelAttribute("changeUsernameForm") ChangeUsernameForm changeUsernameForm, BindingResult result, HttpSession session, ModelMap modelMap) throws Exception {
		UserSession ses = (UserSession) session.getAttribute(UserSession.ID);
		User currentUser = userService.getUser((ses).getUser().getId());
		
		if (!currentUser.getPassword().equals(changeUsernameForm.getPassword())) {
			result.rejectValue("password", "err.invalid_password", "*** Invalid password ***");
			return "user/changeUsername";
		}
		try {
			changeUsernameForm.validate();
		} catch (ValidationFailedException e) {
			result.rejectValue(e.getFieldName(), e.getErrorKey(), e.getParameters(), e.getMessage());
			return "user/changeUsername";
		}
		
		User user = userService.changeUsername(currentUser.getEmail(), changeUsernameForm.getUsername());
		if (user == null) {
			result.rejectValue("username", "err.user_username_already_exists", null, "*** User with this username already registered ***");
			return "user/changeUsername";
		}
		
		addUserToSession(user, session);
		modelMap.addAttribute("usernameChanged", true);
		return "user/changeUsername";
	}
	
	
	
	/* (non-Javadoc)
	 * @see com.sagg.web.controller.UserController#feedback(javax.servlet.http.HttpSession, org.springframework.ui.ModelMap)
	 */
	@RequestMapping(value = "/feedback", method = RequestMethod.GET)
	public String feedback(HttpSession session, ModelMap modelMap) throws Exception {
		UserSession ses = (UserSession) session.getAttribute(UserSession.ID);
		User user = (ses == null || (ses).getUser().getId() == null) ? new User() : userService.getUser((ses).getUser().getId());
		
		FeedbackForm form = new FeedbackForm();
		form.setEmail(user.getEmail());
		
		modelMap.addAttribute("feedbackForm", form);
		return "user/feedback";
	}
	
	/* (non-Javadoc)
	 * @see com.sagg.web.controller.UserController#feedback(com.sagg.web.form.FeedbackForm, org.springframework.validation.BindingResult, javax.servlet.http.HttpSession, org.springframework.ui.ModelMap)
	 */
	@RequestMapping(value = "/feedback", method = RequestMethod.POST)
	public String feedback(@ModelAttribute("feedbackForm") FeedbackForm feedbackForm, BindingResult result, HttpSession session, ModelMap modelMap) throws Exception {
		
		try {
			feedbackForm.validate();
		} catch (ValidationFailedException e) {
			result.rejectValue(e.getFieldName(), e.getErrorKey(), e.getParameters(), e.getMessage());
			return "user/feedback";
		}

		userService.sendFeedback(feedbackForm);
		
		modelMap.addAttribute("feedbackSent", true);
		return "user/feedback";
	}
	
	
	
	private void addUserToSession(User user, HttpSession session /*WebRequest webRequest*/) {
		UserSession userSession = new UserSession();
		userSession.setUser(user);
//		webRequest.setAttribute(UserSession.ID, userSession, WebRequest.SCOPE_SESSION);
		session.setAttribute(UserSession.ID, userSession);
	}
	
	protected boolean validateCaptcha(String captcha, BindingResult result, String sessionId, String errorCode) {
		if (null != result.getFieldError("captcha")) { // If the captcha field is already rejected
			return false;
		}
		boolean validCaptcha = false;
		try {
			validCaptcha = captchaService.validateResponseForID(sessionId, captcha);
		} catch (CaptchaServiceException e) {
			e.printStackTrace();
		}
		if (!validCaptcha) {
			result.rejectValue("captcha", errorCode, errorCode);
		}
		return validCaptcha;
	}

}
