package com.sagg.web.controller;


import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@RequestMapping("/about")
public class AboutController {
	
	@RequestMapping(value = "/show", method = RequestMethod.GET)
	public String show(HttpSession session, ModelMap modelMap) throws Exception {
		return "about/show";
	}

}
