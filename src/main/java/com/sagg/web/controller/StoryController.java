package com.sagg.web.controller;

import java.net.URL;
import java.util.Collection;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sagg.domain.Story;
import com.sagg.domain.User;
import com.sagg.exception.DuplicateStoryException;
import com.sagg.exception.ValidationFailedBatchException;
import com.sagg.exception.ValidationFailedException;
import com.sagg.service.StoryService;
import com.sagg.service.UserService;
import com.sagg.util.Globals;
import com.sagg.util.StringUtil;
import com.sagg.util.URLUtil;
import com.sagg.util.UserSessionUtil;
import com.sagg.web.Authorizable;
import com.sagg.web.UserSession;

@Controller
@RequestMapping("/story")
public class StoryController {

	@Autowired private StoryService storyService;
	@Autowired private UserService userService;

	
//	@RequestMapping(value = "/test", method = RequestMethod.GET)
//	public void test(
//			HttpSession session, HttpServletResponse response, ModelMap modelMap) throws Exception {
//
//		try {
//			String title = "Hello World!";
//			response.getWriter().print(title);
//		} catch (Exception e) {
//			//Ignore errors, since this is an AJAX request.
//		}
//		
//	}
	
	@RequestMapping(value = "/show", method = RequestMethod.GET)
	public String show(
			@RequestParam(required=true, value="id") String compoundId,
			HttpSession session,
			ModelMap modelMap) throws Exception {

		try {
			Integer id = Integer.parseInt(compoundId.substring(compoundId.lastIndexOf("-") + 1));
			Story story = storyService.get(id);
			if (story == null) {
				return "story/list"; // Story not found
			}
			modelMap.addAttribute("story", story); //stories.iterator().next());
		} catch (Exception e) {
			e.printStackTrace();
			return "story/list";
		}
		return "story/show";
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String list(
			@RequestParam(required=false, value="sort") String sort,
			@RequestParam(required=false, value="days") Integer days,
			@RequestParam(required=false, value="show") String show,
			HttpSession session,
			ModelMap modelMap) throws Exception {
		
		User user = UserSessionUtil.refreshUser(session, userService);
		
		Collection<Story> stories;
		if (user != null && "bookmarks".equals(show)) {
			stories = user.getBookmarkedStories();
		} else {
			stories = storyService.list(1, days, sort);
		}
		
		modelMap.addAttribute("stories", stories);
		modelMap.addAttribute("sort", sort);
		modelMap.addAttribute("days", days);
		modelMap.addAttribute("show", show);

		return "story/list";
	}

	@RequestMapping(value = "/ajaxList", method = RequestMethod.GET)
	public String ajaxList(
			@RequestParam(required=false, value="page") Integer page,
			@RequestParam(required=false, value="sort") String sort,
			@RequestParam(required=false, value="days") Integer days,
			@RequestParam(required=false, value="query") String query,
			@RequestParam(required=false, value="show") String show,
			HttpSession session,
			ModelMap modelMap) throws Exception {
		
		//If the user is logged in we will need the user's info for the ajaxList.jsp rendering:
		UserSessionUtil.refreshUser(session, userService);

		Collection<Story> stories = null;
		
		if (StringUtil.isEmpty(show)) {
			stories = (StringUtil.isEmpty(query)) 
					? storyService.list(page, days, sort)
					: storyService.search(query, page);
		}
				
		modelMap.addAttribute("stories", stories);

		return "story/ajaxList";
	}

	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public String search(
			@RequestParam(required=true, value="query") String query,
			@RequestParam(required=false, value="page") Integer page,
			HttpSession session,
			ModelMap modelMap) throws Exception {
		
		Collection<Story> stories = storyService.search(query, page);
		
		modelMap.addAttribute("stories", stories);
		modelMap.addAttribute("query", query);

		return "story/list";
	}

	
	@Authorizable
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public String create(
			@RequestParam(required=false, value="storyId") Integer storyId, // When editing an existing story
			@RequestParam(required=false, value="url") String url,          // Populated by Share button from other sites
			@RequestParam(required=false, value="title") String title,      // Populated by Share button from other sites
			@RequestParam(required=false, value="desc") String description, // Populated by Share button from other sites
			HttpSession session,
			ModelMap modelMap) throws Exception {
		
		Story story = null;
		
		if (storyId != null) {
			//Edit existing story, if exists
			story = storyService.get(storyId);
		}
		if (story == null) {
			//Create new story
			story = new Story(url, title, description);
		}
		
		modelMap.addAttribute("story", story);
		modelMap.addAttribute("maxUrlLength", Globals.MAX_STORY_URL_LENGTH);
		modelMap.addAttribute("maxTitleLength", Globals.MAX_STORY_TITLE_LENGTH);
		modelMap.addAttribute("maxDescriptionLength", Globals.MAX_STORY_DESCRIPTION_LENGTH);

		return "story/create";
	}

	@Authorizable
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String create(
			Story story,
			BindingResult result,
			HttpSession session,
			ModelMap modelMap) throws Exception {

		String url = URLUtil.normalizeUrl(story.getUrl());
		story.setUrl(url);
		
		try {
			story.validate();
		} catch (ValidationFailedBatchException ex) {
			for (ValidationFailedException e : ex.getExceptions()) {
				result.rejectValue(e.getFieldName(), e.getErrorKey(), e.getParameters(), e.getMessage());
			}
			return "story/create";
		}
		
		UserSession userSession = (UserSession) session.getAttribute(UserSession.ID);
		story.setCreatedBy(userService.getUser((userSession).getUser().getId()));

		try {
			Story savedStory = (story.getId() == null) ? storyService.save(story) : storyService.update(story);
			if (savedStory == null) {
				return "story/create";
			}
		} catch (DuplicateStoryException e) {
			result.rejectValue("url", "err.url_already_exists", new String[] { url }, "*** Specified URL already exists ***");
			return "story/create";
		} catch (Exception e) {  
			e.printStackTrace(); //TODO: use logger
			return "redirect:list"; //Something went terribly wrong, we don't know what to do, so just redirect user to the main page.
		}
		
		return "redirect:list";
	}
	

	@Authorizable
	@RequestMapping(value = "/vote", method = RequestMethod.GET)
	public String vote(
			@RequestParam(required=true, value="storyId") Integer storyId,
			@RequestParam(required=true, value="direction") Integer direction,
			HttpSession session,
			ModelMap modelMap) throws Exception {

		User user = UserSessionUtil.refreshUser(session, userService);
		Story story = storyService.vote(user, storyId, direction);
		modelMap.addAttribute("story", story);
		return "story/vote";
	}
	
	@Authorizable
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public String delete(
			@RequestParam(required=false, value="redirect") String redirect,
			@RequestParam(required=true, value="storyId") Integer storyId,
			HttpSession session,
			ModelMap modelMap) throws Exception {
		
		User user = UserSessionUtil.refreshUser(session, userService);
		Story story = storyService.get(storyId);
		
		if (story != null && story.getCreatedBy() != null && story.getCreatedBy().getEmail().equals(user.getEmail())) {
			storyService.delete(story);
		}
		
		return redirect == null ? "redirect:list" : "redirect:" + redirect;
	}
	
	@Authorizable
	@RequestMapping(value = "/flag", method = RequestMethod.GET)
	public void flag(
			@RequestParam(required=true, value="storyId") Integer storyId,
			HttpSession session,
			ModelMap modelMap) throws Exception {
		/*
		 * TODO: Do we want to link spam flaggings with users to prevent the same
		 * user from flagging twice?
		 */
		Story story = storyService.flagAsSpam(storyId);
		modelMap.addAttribute("story", story);
	}
	
	@Authorizable
	@RequestMapping(value = "/bookmark", method = RequestMethod.GET)
	public void bookmark(
			@RequestParam(required=false, value="storyId") Integer storyId,
			@RequestParam(required=false, value="bookmarkId") Integer bookmarkId,
			HttpSession session,
			HttpServletResponse response) throws Exception {

		User user = UserSessionUtil.refreshUser(session, userService);

		bookmarkId = storyService.bookmark(user, storyId, bookmarkId);
		
		if (bookmarkId != null) {
			response.getWriter().print(bookmarkId);
		}
	}
	
	@Authorizable
	@RequestMapping(value = "/getTitle", method = RequestMethod.GET)
	public void getTitle(
			@RequestParam(required=true, value="url") String url,
			HttpSession session, HttpServletResponse response, ModelMap modelMap) throws Exception {

		try {
			String title = URLUtil.getTitle(new URL(URLUtil.normalizeUrl(url)));
			response.getWriter().print(title);
		} catch (Exception e) {
			//Ignore errors, since this is an AJAX request.
		}
		
	}

}
