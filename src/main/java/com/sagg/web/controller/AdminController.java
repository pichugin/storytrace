package com.sagg.web.controller;

import java.util.Collection;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sagg.domain.ResultSet;
import com.sagg.domain.Story;
import com.sagg.domain.User;
import com.sagg.service.AdminService;
import com.sagg.service.StoryService;
import com.sagg.service.UserService;
import com.sagg.util.Globals;
import com.sagg.web.Authorizable;


@Controller
@RequestMapping("/admin")
public class AdminController {
	
	@Autowired private UserService userService;
	@Autowired private StoryService storyService;
	@Autowired private AdminService adminService;
	
	@Authorizable(userType = Globals.USER_TYPE_ADMIN)
	@RequestMapping(value = "/show", method = RequestMethod.GET)
	public String show(HttpSession session, ModelMap modelMap) throws Exception {

		//TODO: limit the number of selected users
		Collection<User> users = userService.getUsers();
		modelMap.addAttribute("users", users);
		
		//TODO: limit the number of selected stories
		Collection<Story> stories = storyService.getStories();
		modelMap.addAttribute("stories", stories);
		
		return "admin/show";
	}

	@Authorizable(userType = Globals.USER_TYPE_ADMIN)
	@RequestMapping(value = "/executeQuery", method = RequestMethod.GET)
	public String executeQuery(
			@RequestParam(required = true) String query,
			HttpSession session, ModelMap modelMap) throws Exception {
		
		try {
			ResultSet resultSet = adminService.executeQuery(query);
			modelMap.addAttribute("resultSet", resultSet);
		} catch (Exception e) {
			modelMap.addAttribute("resultSet", null);
			modelMap.addAttribute("sqlError", e.getCause().getCause());
		}
		
		return "admin/executeQuery";
	}

}
