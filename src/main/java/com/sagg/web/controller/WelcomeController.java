package com.sagg.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WelcomeController {

	/**
     * @see WEB-INF/jsp/welcome.jsp
     * @see WEB-INF/jsp/index.jsp
     */
    @RequestMapping
    public void welcome() {
    }
    
}
