package com.sagg.web.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.octo.captcha.service.CaptchaServiceException;
import com.octo.captcha.service.image.ImageCaptchaService;
import com.sagg.web.UserSession;

@Controller
@RequestMapping("/captcha")
public class CaptchaController {

	public static final String CAPTCHA_IMAGE_FORMAT = "jpeg";

	@Autowired
	private ImageCaptchaService captchaService;


	@RequestMapping(value = "generateImage", method = RequestMethod.GET)
	public void generateImage(HttpServletResponse response, HttpSession session) throws Exception {
		byte[] captchaChallengeAsJpeg = null;
		// the output stream to render the captcha image as jpeg into
		ByteArrayOutputStream jpegOutputStream = new ByteArrayOutputStream();  
		try {
			// get the session id that will identify the generated captcha.
			// the same id must be used to validate the response, the session id is a good candidate!

			String captchaId = session.getId(); //request.getSession().getId();
			BufferedImage challenge = captchaService.getImageChallengeForID(captchaId); //, request.getLocale());  

			ImageIO.write(challenge, CAPTCHA_IMAGE_FORMAT, jpegOutputStream);
		} catch (IllegalArgumentException e) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);  
			return;
		} catch (CaptchaServiceException e) {
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);  
			return;
		}

		captchaChallengeAsJpeg = jpegOutputStream.toByteArray();  

		// flush it in the response  
		response.setHeader("Cache-Control", "no-store");  
		response.setHeader("Pragma", "no-cache");  
		response.setDateHeader("Expires", 0);  
		response.setContentType("image/"+CAPTCHA_IMAGE_FORMAT);  

		ServletOutputStream responseOutputStream = response.getOutputStream();  
		responseOutputStream.write(captchaChallengeAsJpeg);  
		responseOutputStream.flush();  
		responseOutputStream.close();

	}

}
