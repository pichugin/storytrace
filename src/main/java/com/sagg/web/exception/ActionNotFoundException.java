package com.sagg.web.exception;

public class ActionNotFoundException extends Exception {

	public ActionNotFoundException(String message) {
		super(message);
	}

}
