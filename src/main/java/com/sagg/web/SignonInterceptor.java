package com.sagg.web;

import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.WebUtils;

import com.sagg.web.exception.ActionNotFoundException;
import com.sagg.web.exception.AuthenticationException;
import com.sagg.web.exception.AuthorizationException;


public class SignonInterceptor extends HandlerInterceptorAdapter {
	
	public static final String URL_LOGIN = "../user/loginex";
	public static final String URL_HOME_PAGE = "/main/story/list";
	
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		
		request.setCharacterEncoding("UTF-8");
		
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, UserSession.ID);
		
		String url = request.getPathInfo();
		String query = request.getQueryString();
		
		//TODO: The following parameter doesn't have anything to do with the Sign On process, so maybe create a separate filter for it?
		//I don't like this solution, but we use this value on some pages as the redirect parameter's value: 
		request.getSession().setAttribute("actionPath", url);
		
		if (url == null) { // This happens when we type in www.storytrace.com/main/abracadabra (Error 500)
			throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView(request.getContextPath() + URL_HOME_PAGE)));
		}
		
		try {
			checkAccess(userSession, handler.getClass(), url, request.getMethod());
		} catch (ActionNotFoundException e) {
			throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView(request.getContextPath() + URL_HOME_PAGE)));	
		} catch (AuthorizationException e) {
			throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView(request.getContextPath() + URL_HOME_PAGE)));	
		} catch (AuthenticationException e) {
			ModelAndView modelAndView = new ModelAndView(new RedirectView(URL_LOGIN));
			url = (query != null && !"".equals(query)) ? url + "?" + query : url;
			modelAndView.addObject("signonForwardAction", url);
			throw new ModelAndViewDefiningException(modelAndView);
		}

		return true;
	}
	
	private void checkAccess(UserSession userSession, Class<?> handlerClass, String url, String httpMethod) throws Exception {
		boolean authenticated = userSession != null && userSession.isValid();
		Authorizable authAnnotation = getHandlerMethod(handlerClass, url, httpMethod).getAnnotation(Authorizable.class);
		if (authAnnotation == null) {
			return;
		}
		if (!authenticated) {
			throw new AuthenticationException();
		}
		if (userSession.getUser().getType().intValue() > authAnnotation.userType()) {
			throw new AuthorizationException();
		}
	}
	
	// TODO: Modify @Authorizable to have a parameter that holds the HTTP method value (@Authorizable(method=GET))
	private Method getHandlerMethod(Class<?> clazz, String url, String httpMethod) throws Exception {
		String[] urlParts = url.split("/");
		int indexAdjustment = url.startsWith("/") ? 1 : 0;
		if (urlParts.length < 3 - indexAdjustment) {
			throw new ActionNotFoundException("The URL has an incorrect format: '" + url + "'");
		}
		String methodName = urlParts[urlParts.length - 1 + indexAdjustment];
		for (Method method : clazz.getMethods()) {
			if (method.getName().equals(methodName) 
					&& method.isAnnotationPresent(RequestMapping.class)) {
				RequestMapping reqMappingAnn = method.getAnnotation(RequestMapping.class);
				if (reqMappingAnn.method().length > 0) {
					for (RequestMethod m : reqMappingAnn.method()) {
						if (httpMethod.equals(m.toString())) {
							return method;
						}
					}
				}
			}
		}
		throw new ActionNotFoundException(String.format("The method '%s' was not found in the '%s' controller!", 
				methodName, clazz.getName()));
	}
	
}
