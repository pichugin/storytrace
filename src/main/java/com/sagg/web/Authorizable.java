package com.sagg.web;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.sagg.util.Globals;

/**
 * This interface is just an indication that the controller requires authorization
 * */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE, ElementType.METHOD })
public @interface Authorizable {
	int userType() default Globals.USER_TYPE_REGULAR;
}
