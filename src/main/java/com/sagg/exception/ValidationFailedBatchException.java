package com.sagg.exception;

import java.util.ArrayList;
import java.util.List;

public class ValidationFailedBatchException extends Exception {

	private List<ValidationFailedException> exceptions = new ArrayList<ValidationFailedException>();

	
	public ValidationFailedBatchException() {
	}

	public ValidationFailedBatchException(List<ValidationFailedException> exceptions) {
		this.exceptions = exceptions;
	}

	public List<ValidationFailedException> getExceptions() {
		return exceptions;
	}
	
	public void addException(ValidationFailedException exception) {
		exceptions.add(exception);
	}
	
}
