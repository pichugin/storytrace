package com.sagg.exception;

public class ValidationFailedException extends Exception {

	private static final long serialVersionUID = 1L;
	private String fieldName;
	private String errorKey;
	private String defaultErrorMessage;
	private String[] parameters;

	public ValidationFailedException(String fieldName, String errorKey, String defaultErrorMessage) {
		this.fieldName = fieldName;
		this.errorKey = errorKey;
		this.defaultErrorMessage = defaultErrorMessage;
	}
	
	public ValidationFailedException(String fieldName, String errorKey, String[] parameters, String defaultErrorMessage) {
		this(fieldName, errorKey, defaultErrorMessage);
		this.parameters = parameters;
	}
	
	public String getFieldName() {
		return fieldName;
	}

	public String getErrorKey() {
		return errorKey;
	}

	public String getMessage() {
		return defaultErrorMessage;
	}

	public String[] getParameters() {
		return parameters;
	}

}
