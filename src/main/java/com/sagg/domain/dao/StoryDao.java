package com.sagg.domain.dao;

import java.util.Collection;
import java.util.Date;

import com.sagg.domain.Story;

public interface StoryDao {
	
	Collection<Story> find(String url) throws Exception;
	Collection<Story> find(int page, int pageSize, Date sinceDate, String sort) throws Exception;
	Collection<Story> getStories() throws Exception;
	void delete(Integer storyId) throws Exception;
	
}
