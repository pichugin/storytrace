package com.sagg.domain.dao;

import com.sagg.domain.Activation;

public interface ActivationDao {
	
	Activation find(String uuid) throws Exception;

}
