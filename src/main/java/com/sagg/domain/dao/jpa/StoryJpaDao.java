package com.sagg.domain.dao.jpa;

import java.util.Collection;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sagg.domain.Bookmark;
import com.sagg.domain.Story;
import com.sagg.domain.Vote;
import com.sagg.domain.dao.StoryDao;

@Repository
@Transactional
public class StoryJpaDao implements StoryDao {
	
	public static final String SORT_BY_VOTE = "vote";
	public static final String SORT_BY_DATE = "date";

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	public Collection<Story> find(String url) throws Exception {
		return (Collection<Story>) entityManager.createQuery(
				"from Story story where story.url = :url").setParameter("url", url).getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public Collection<Story> getStories() throws Exception {
		return (Collection<Story>) entityManager.createQuery(
				"from Story story").getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public Collection<Story> find(int page, int pageSize, Date sinceDate, String sort) throws Exception {
		String orderBy = " order by ";
		if (SORT_BY_VOTE.equals(sort)) {
			orderBy += "story.voteUp";
		} else /*if (SORT_BY_DATE.equals(sort))*/ { // Sort by date by default
			orderBy += "story.created";
		}
		String where = sinceDate != null ? "where story.created >= :since" : "";
		Query query = entityManager.createQuery("from Story story " + where + orderBy + " desc");
		if (sinceDate != null) {
			query.setParameter("since", sinceDate);
		}
		query.setFirstResult(pageSize * (page - 1));
		query.setMaxResults(pageSize);
		return (Collection<Story>) query.getResultList();
	}
	
	@Override
	public void delete(Integer storyId) throws Exception {
		
		//TODO: maybe optimize the following block, or move it somewhere else...
		//Delete story votes:
		@SuppressWarnings("unchecked")
		Collection<Vote> votes = entityManager.createQuery(
			"from Vote vote where vote.story.id = :story_id").setParameter(
					"story_id", storyId).getResultList();
		for (Vote vote : votes) {
			entityManager.remove(vote);
		}

		//TODO: maybe optimize the following block, or move it somewhere else...
		//Delete bookmarks that refer to the story being deleted:
		@SuppressWarnings("unchecked")
		Collection<Bookmark> bookmarks = entityManager.createQuery(
			"from Bookmark bookmark where bookmark.story.id = :story_id").setParameter(
					"story_id", storyId).getResultList();
		for (Bookmark bookmark : bookmarks) {
			entityManager.remove(bookmark);
		}

		//Delete the story:
		Story toRemove = entityManager.find(Story.class, storyId);
		entityManager.remove(toRemove);
		entityManager.flush();
	}

}
