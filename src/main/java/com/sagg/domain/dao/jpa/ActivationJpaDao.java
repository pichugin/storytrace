package com.sagg.domain.dao.jpa;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sagg.domain.Activation;
import com.sagg.domain.dao.ActivationDao;

@Repository
@Transactional
public class ActivationJpaDao implements ActivationDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public Activation find(String uuid) throws Exception {
		Collection<Activation> activations = (Collection<Activation>) entityManager.createQuery(
				"from Activation activation where activation.uuid = :uuid").setParameter("uuid", uuid).getResultList();
		return activations == null || activations.isEmpty() ? null : activations.iterator().next();
	}

}
