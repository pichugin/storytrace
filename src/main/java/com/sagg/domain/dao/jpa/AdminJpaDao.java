package com.sagg.domain.dao.jpa;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sagg.domain.dao.AdminDao;

@Repository
@Transactional
public class AdminJpaDao implements AdminDao {

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	public Collection<Object> select(String query) throws Exception {
		return entityManager.createNativeQuery(query).getResultList();
//		return (Collection<DomainObject>) entityManager.createQuery(query).getResultList();
	}

}
