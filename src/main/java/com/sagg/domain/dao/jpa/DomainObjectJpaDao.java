package com.sagg.domain.dao.jpa;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sagg.domain.DomainObject;
import com.sagg.domain.dao.DomainObjectDao;

@Repository
@Transactional
public class DomainObjectJpaDao implements DomainObjectDao {
	
	@PersistenceContext
	private EntityManager entityManager;

	public DomainObject get(Integer id, Class<? extends DomainObject> domainObjectClass) throws Exception {
		return entityManager.find(domainObjectClass, id);
	}
	
	@Transactional
	public void delete(Integer id, Class<? extends DomainObject> domainObjectClass) throws Exception {
		DomainObject toRemove = get(id, domainObjectClass);
		entityManager.remove(toRemove);
	}

	@Transactional
	public DomainObject save(DomainObject domainObject) throws Exception {
		DomainObject mergedDomainObject = entityManager.merge(domainObject);
		entityManager.flush();
		return mergedDomainObject;
	}

}
