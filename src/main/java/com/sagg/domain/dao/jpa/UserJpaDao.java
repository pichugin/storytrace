package com.sagg.domain.dao.jpa;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sagg.domain.User;
import com.sagg.domain.dao.UserDao;

@Repository
@Transactional
public class UserJpaDao implements UserDao {
	
	@PersistenceContext
	private EntityManager entityManager;

	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public User getUserByEmail(String email) throws DataAccessException {
		Collection<User> users = (Collection<User>) entityManager.createQuery("from User user where user.email = :email")
				.setParameter("email", email).getResultList();
		return users == null || users.isEmpty() ? null : users.iterator().next();
	}
	
	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public User getUserByUsername(String username) throws DataAccessException {
		Collection<User> users = (Collection<User>) entityManager.createQuery("from User user where user.username = :username")
				.setParameter("username", username).getResultList();
		return users == null || users.isEmpty() ? null : users.iterator().next();
	}

	public void deleteUser(User user) throws Exception {
		entityManager.remove(user);
	}

	/** Test: {@link UserJpaDaoTest#testGetUser()}*/
	@Transactional(readOnly = true)
	public User getUser(Integer id) throws Exception {
		return (User) entityManager.find(User.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public Collection<User> getUsers() throws Exception {
		return (Collection<User>) entityManager.createQuery("from User user").getResultList();
	}

	@Transactional /*(readOnly = true)*/
	public User save(User user) throws Exception {
		User mergedUser = entityManager.merge(user);
		entityManager.flush();
		user.setId(mergedUser.getId());
//		entityManager.persist(user);
		return mergedUser;
	}

}
