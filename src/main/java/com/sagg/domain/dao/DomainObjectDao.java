package com.sagg.domain.dao;

import com.sagg.domain.DomainObject;

public interface DomainObjectDao {

	DomainObject get(Integer id, Class<? extends DomainObject> domainObjectClass) throws Exception;
	void delete(Integer id, Class<? extends DomainObject> domainObjectClass) throws Exception;
	DomainObject save(DomainObject domainObject) throws Exception;

}
