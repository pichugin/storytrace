package com.sagg.domain.dao;

import java.util.Collection;

import com.sagg.domain.User;

public interface UserDao {
	
	User save(User user) throws Exception;
	User getUser(Integer id) throws Exception;
	User getUserByEmail(String email) throws Exception;
	User getUserByUsername(String username) throws Exception;
	Collection<User> getUsers() throws Exception;
	void deleteUser(User user) throws Exception;

}
