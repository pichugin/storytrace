package com.sagg.domain.dao;

import java.util.Collection;


public interface AdminDao {

	Collection</*DomainObject*/Object> select(String query) throws Exception;

}