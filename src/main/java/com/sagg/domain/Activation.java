package com.sagg.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="ACTIVATION")
public class Activation extends DomainObject implements Serializable {

	private static final long serialVersionUID = 1L;
	private String uuid;
	private User user;
	
	public Activation() {
	}
	
	public Activation(User user, String uuid) {
		this.user = user;
		this.uuid = uuid;
	}

	public String getUuid() {
		return uuid;
	}
	
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	
	@OneToOne
	public User getUser() {
		return user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
	
}
