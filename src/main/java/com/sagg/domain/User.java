package com.sagg.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.sagg.exception.ValidationFailedException;
import com.sagg.util.Globals;
import com.sagg.util.EmailUtil;
import com.sagg.util.StringUtil;

@Entity
@Table(name="USERS")
public class User extends DomainObject implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String email;
	private String username;
	private String password;
	private Integer type;
	private Boolean active;
	private Collection<Vote> votes;
	private Collection<Story> stories;
	private Collection<Bookmark> bookmarks;
    
    public User() {
    	type = Globals.USER_TYPE_REGULAR;
    }

	@Column(name = "EMAIL", unique=true, nullable = false, length = Globals.MAX_USER_EMAIL_LENGTH)
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = (email != null) ? email.trim() : null;
	}
	
	@Column(name = "USERNAME", unique=true, nullable = true, length = Globals.MAX_USER_USERNAME_LENGTH)
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String name) {
		this.username = (name != null) ? name.trim() : null;
	}
	
	@Column(name = "PASSWORD", nullable = false, length = Globals.MAX_USER_PASSWORD_LENGTH)
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Column(name = "TYPE", nullable = false)
	public Integer getType() {
		return type;
	}
	
	public void setType(Integer type) {
		this.type = type;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
	
	@OneToMany(cascade = {CascadeType.MERGE, CascadeType.REMOVE, CascadeType.REFRESH}, fetch=FetchType.EAGER, mappedBy="user")
	//TODO: JPA 2.0 supports new annotation: @MapKeyColumn(name="story_id") to create a Map<story_id, Vote> rather than a set
	public Collection<Vote> getVotes() {
		return votes;
	}
	
	//TODO: Mmm... think harder about redesigning this approach :(
	@Transient
	public Map<Integer, Vote> getVotesMap() { // Map<storyId, vote>
		return new HashMap<Integer, Vote>() {
			private static final long serialVersionUID = 1L;
			public Vote get(Object storyId) {
				Collection<Vote> votes = getVotes();
				if (votes != null) {
					for (Vote vote : votes) {
						if (storyId.equals(vote.getStory().getId())) {
							return vote;
						}
					}
				}
				return null;
			}
		};
	}
	
	@Transient
	public Collection<Vote> getVotesUp() {
		Collection<Vote> votesUp = new ArrayList<Vote>();
		if (votes != null) {
			for (Vote vote : votes) {
				if (vote.getVoteDirection().equals(Globals.VOTE_UP)) {
					votesUp.add(vote);
				}
			}
		}
		return votesUp;
	}
	
	@Transient
	public Collection<Vote> getVotesDown() {
		Collection<Vote> votesDown = new ArrayList<Vote>();
		if (votes != null) {
			for (Vote vote : votes) {
				if (vote.getVoteDirection().equals(Globals.VOTE_DOWN)) {
					votesDown.add(vote);
				}
			}
		}
		return votesDown;
	}

	public void setVotes(Collection<Vote> votes) {
		this.votes = votes;
	}

	@OneToMany(cascade = CascadeType.ALL, mappedBy="createdBy")
	public Collection<Story> getStories() {
		return stories;
	}
	
	public void setStories(Collection<Story> stories) {
		this.stories = stories;
	}
	
	@OneToMany(cascade = /*CascadeType.ALL*/ {CascadeType.MERGE, CascadeType.REMOVE, CascadeType.REFRESH}, fetch=FetchType.LAZY, mappedBy="user")
	public Collection<Bookmark> getBookmarks() {
		return bookmarks;
	}

	public void setBookmarks(Collection<Bookmark> bookmarks) {
		this.bookmarks = bookmarks;
	}
	
	@Transient
	public Collection<Story> getBookmarkedStories() {
		Collection<Story> stories = new ArrayList<Story>();
		for (Bookmark bookmark : bookmarks) {
			stories.add(bookmark.getStory());
		}
		return stories;
	}
	
	@Transient
	public Map<Integer, Bookmark> getBookmarksMap() {
		return new HashMap<Integer, Bookmark>() {
			private static final long serialVersionUID = 1L;
			public Bookmark get(Object storyId) {
				Collection<Bookmark> bookmarks = getBookmarks();
				if (bookmarks != null) {
					for (Bookmark bookmark : bookmarks) {
						if (storyId.equals(bookmark.getStory().getId())) {
							return bookmark;
						}
					}
				}
				return null;
			}
		};
	}

	//TODO: Move the validation methods out of the domain objects (UserValidator)
	//      This would make the code cleaner and allow us to have different validators for 
	//      the same object - it may be helpful sometimes. 
	public void validate() throws ValidationFailedException {
		if (!EmailUtil.isValidEmail(this.email)) {
			throw new ValidationFailedException("user.email", "err.email_invalid", "*** Invalid email. ***");
		}
		if (this.email.length() > Globals.MAX_USER_EMAIL_LENGTH) {
			throw new ValidationFailedException("user.email", "err.email_too_long", new String[]{""+Globals.MAX_USER_EMAIL_LENGTH}, "*** Email is too long. ***");
		}
		if (StringUtil.isEmpty(this.password)) {
			throw new ValidationFailedException("user.password", "err.password_empty", "*** Password can not be empty. ***");
		}
		if (this.password.length() > Globals.MAX_USER_PASSWORD_LENGTH) {
			throw new ValidationFailedException("user.password", "err.password_too_long", new String[]{""+Globals.MAX_USER_PASSWORD_LENGTH}, "*** Password is too long. ***");
		}
	}
	
	public String toString() {
		return this.email;
	}

}
