package com.sagg.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="BOOKMARK")
public class Bookmark extends DomainObject implements Serializable {

	private static final long serialVersionUID = 1L;
	private User user;
	private Story story;

	
	public Bookmark() {
	}

	public Bookmark(User user, Story story) {
		this.user = user;
		this.story = story;
	}

	@ManyToOne
	@JoinColumn(name = "user_id")
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	@ManyToOne
	@JoinColumn(name = "story_id")
	public Story getStory() {
		return story;
	}

	public void setStory(Story story) {
		this.story = story;
	}
	
}
