package com.sagg.domain;

import java.util.Collection;

public class ResultSet {
	
	private Collection<String> columnNames;
	private Collection<Object> rows;
	
	public ResultSet(Collection<String> columnNames, Collection<Object> rows) {
		this.columnNames = columnNames;
		this.rows = rows;
	}

	public Collection<String> getColumnNames() {
		return columnNames;
	}

	public Collection<Object> getRows() {
		return rows;
	}
	
}
