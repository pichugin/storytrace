package com.sagg.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.compass.annotations.Searchable;
import org.compass.annotations.SearchableId;
import org.compass.annotations.SearchableProperty;

import com.sagg.exception.ValidationFailedBatchException;
import com.sagg.exception.ValidationFailedException;
import com.sagg.util.Globals;
import com.sagg.util.StringUtil;
import com.sagg.util.URLUtil;

@Entity
@Table(name="STORY")
@Searchable
public class Story extends DomainObject implements Serializable {

	private static final long serialVersionUID = 1L;
	private String url;
	private String title;
	private String description;
	private String thumbnailUrl;
	//private Collection<Tag> tags;
	private int voteUp;
	private int voteDown;
	private int spamFlags;
	private User createdBy;
	
	private Date created;
	private Date updated;
	
//    @Id
//    @GeneratedValue(strategy=GenerationType.AUTO)
//    @Column(name = "ID")
//    @SearchableId
//	public Integer getId() {
//		return id;
//	}
	
	public Story() {
	}

	public Story(int id, String url, String title, String description) {
		this(url, title, description);
		setId(id);
	}

	public Story(String url, String title, String description) {
		this.url = url;
		this.title = title;
		this.description = description;
	}

	@Column(name = "URL", nullable = false, length = Globals.MAX_STORY_URL_LENGTH)
	@SearchableProperty
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	@Column(name = "TITLE", nullable = false, length = Globals.MAX_STORY_TITLE_LENGTH)
	@SearchableProperty
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	@Column(name = "DESCRIPTION", nullable = false, length = Globals.MAX_STORY_DESCRIPTION_LENGTH)
	@SearchableProperty
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "THUMBNAIL_URL", nullable = true, length = Globals.MAX_STORY_THUMBNAIL_URL_LENGTH)
	public String getThumbnailUrl() {
		return thumbnailUrl;
	}
	
	public void setThumbnailUrl(String thumbnailUrl) {
		this.thumbnailUrl = thumbnailUrl;
	}

	public int getVoteUp() {
		return voteUp;
	}

	public void setVoteUp(int voteUp) {
		this.voteUp = voteUp;
	}

	public int getVoteDown() {
		return voteDown;
	}

	public void setVoteDown(int voteDown) {
		this.voteDown = voteDown;
	}

	public int getSpamFlags() {
		return spamFlags;
	}

	public void setSpamFlags(int spamFlags) {
		this.spamFlags = spamFlags;
	}

	@ManyToOne
	@JoinColumn(name = "user_id")
	public User getCreatedBy() {
		return createdBy;
	}
	
	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	@PrePersist
	public void onCreate() {
		this.created = new Date();
	}

	@Temporal(TemporalType.TIMESTAMP)
	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	@PreUpdate
	public void onUpdate() {
		this.updated = new Date();
	}

	@Temporal(TemporalType.TIMESTAMP)
	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	
	@Transient
	public String getCompoundId() {
		return ((title == null ? "" : title.replaceAll("\\W", "-")).toLowerCase() + "-" + id).replaceAll("[-]+", "-"); 
	}
	
	@Transient
	public String getRandomUuid() {
		return UUID.randomUUID().toString();
	}
	
	public void validate() throws ValidationFailedBatchException {
		ValidationFailedBatchException exceptions = new ValidationFailedBatchException();
		if (StringUtil.isEmpty(this.url)) {
			exceptions.addException(new ValidationFailedException("url", "err.url_empty", "*** URL can not be empty. ***"));
			
		} else if (this.url.length() > Globals.MAX_STORY_URL_LENGTH) {
			exceptions.addException(new ValidationFailedException("url", "err.url_too_long", 
					new String[] { "" + Globals.MAX_STORY_URL_LENGTH }, "*** URL is too long. ***"));
			
		} else if (!URLUtil.isValidUrl(url)) {
			exceptions.addException(new ValidationFailedException("url", "err.url_invalid", 
					new String[] { url }, "*** Invalid URL ***"));
		}
		if (StringUtil.isEmpty(this.title)) {
			exceptions.addException(new ValidationFailedException("title", "err.title_empty", "*** Title can not be empty. ***"));
		}
		if (this.title.length() > Globals.MAX_STORY_TITLE_LENGTH) {
			exceptions.addException(new ValidationFailedException("title", "err.title_too_long", 
					new String[] { "" + Globals.MAX_STORY_TITLE_LENGTH }, "*** Title is too long. ***"));
		}
		if (StringUtil.isEmpty(this.description)) {
			exceptions.addException(new ValidationFailedException("description", "err.description_empty", 
					"*** Description can not be empty. ***"));
		}
		if (this.description.length() > Globals.MAX_STORY_DESCRIPTION_LENGTH) {
			exceptions.addException(new ValidationFailedException("description", "err.description_too_long", 
					new String[] { "" + Globals.MAX_STORY_DESCRIPTION_LENGTH }, "*** Description is too long. ***"));
		}
	}

}
