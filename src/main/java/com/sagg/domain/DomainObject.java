package com.sagg.domain;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import com.sagg.util.Dumpable;

@MappedSuperclass
public abstract class DomainObject implements Dumpable {

	protected Integer id;
	
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "ID")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
}
