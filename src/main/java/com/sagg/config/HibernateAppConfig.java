package com.sagg.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.compass.annotations.config.CompassAnnotationsConfiguration;
import org.compass.core.Compass;
import org.compass.core.CompassException;
import org.compass.core.CompassIndexSession;
import org.compass.core.CompassQueryBuilder;
import org.compass.core.CompassQueryFilterBuilder;
import org.compass.core.CompassSearchSession;
import org.compass.core.CompassSession;
import org.compass.core.ResourceFactory;
import org.compass.core.config.CompassConfiguration;
import org.compass.core.config.CompassEnvironment;
import org.compass.core.config.CompassSettings;
import org.compass.core.engine.SearchEngineIndexManager;
import org.compass.core.engine.SearchEngineOptimizer;
import org.compass.core.engine.spellcheck.SearchEngineSpellCheckManager;
import org.compass.gps.CompassGps;
import org.compass.gps.device.jpa.JpaGpsDevice;
import org.compass.gps.device.jpa.extractor.SpringNativeJpaExtractor;
import org.compass.gps.impl.SingleCompassGps;
import org.compass.spring.transaction.SpringSyncTransactionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver;
import org.springframework.instrument.classloading.LoadTimeWeaver;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import static com.sagg.config.Util.*;

@Configuration
//@AnnotationDrivenConfig
//@AnnotationDrivenTx
@PropertySource(name = "dbProperties", value = "classpath:/jdbc.properties")
@Import({AppConfig.class, ExternalDataSourceConfig.class, MailConfig.class})
public class HibernateAppConfig {
    @Autowired Environment env;
        
    private @Autowired DataSource dataSource;
    
	public @Bean PlatformTransactionManager transactionManager() {
		JpaTransactionManager txManager = new JpaTransactionManager();
		txManager.setEntityManagerFactory(entityManagerFactory());
System.out.println("######## ENABLING TRANSACTION MANAGER");			
		return txManager;
	}
    
    public @Bean EntityManagerFactory entityManagerFactory() {
        Properties jpaProperties = new Properties();
        populateProperty(jpaProperties, env, "hibernate.dialect");
        populateProperty(jpaProperties, env, "hibernate.show_sql");
        populateProperty(jpaProperties, env, "hibernate.generate_statistics");
        populateProperty(jpaProperties, env, "hibernate.hbm2ddl.auto");
        
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setJpaProperties(jpaProperties);
        em.setDataSource(dataSource/*()*/);
        em.setJpaVendorAdapter(jpaVendorAdapter());
        em.setLoadTimeWeaver(loadTimeWeaver());
        
        em.afterPropertiesSet();
        return em.getObject(); //getNativeEntityManagerFactory(); //Having problems? Try em.getObject();
    }
    
    /**
     * JpaVendorAdapter implementation for Hibernate EntityManager.
     */
    public @Bean JpaVendorAdapter jpaVendorAdapter() {
        HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
        adapter.setShowSql(Boolean.parseBoolean(env.getProperty("jpa.showSql"))); //showJpaSql));
        adapter.setDatabasePlatform(env.getProperty("hibernate.dialect"));
        return adapter;
    }

    /**
     * LoadTimeWeaver relying on VM Instrumentation.
     */
    public @Bean LoadTimeWeaver loadTimeWeaver() {
        return new InstrumentationLoadTimeWeaver();
    }
    
//	public @Bean CompassGps compassGps() {
//		CompassGps gps = new SingleCompassGps(compass());
//		JpaGpsDevice jpaGpsDevice = new JpaGpsDevice("jpaDevice", entityManagerFactory());
//		jpaGpsDevice.setNativeExtractor(new SpringNativeJpaExtractor());
//		gps.addGpsDevice(jpaGpsDevice);
//		gps.start();
//		return gps;
//	}
	
/*    // Temporarily turn off the compass feature...
	public @Bean Compass compass() throws Exception {
		CompassConfiguration conf = new CompassAnnotationsConfiguration()
				.setSetting(CompassEnvironment.CONNECTION, "/tmp/storytrace-index") // "jdbc:hsqldb:hsql://localhost:9001")
//				.setSetting("compass.engine.store.jdbc.dialect", "org.hibernate.dialect.HSQLDialect")
//				.setSetting("compass.engine.maxBufferedDocs", 1000)
//				.setSetting("compass.engine.mergeFactor", 1000)
				.setSetting(CompassEnvironment.Transaction.FACTORY, "org.compass.spring.transaction.SpringSyncTransactionFactory")
				.addScan("com.sagg.domain");
//				.addClass(Story.class);

//		LocalCompassBean lcb = new LocalCompassBean();
//		lcb.setCompassConfiguration(conf);
//		lcb.setTransactionManager(transactionManager());
		
		SpringSyncTransactionFactory.setTransactionManager(transactionManager());
		
		Compass compass = conf.buildCompass();
		
		//Init Gps
		CompassGps gps = new SingleCompassGps(compass);
		JpaGpsDevice jpaGpsDevice = new JpaGpsDevice("jpaDevice", entityManagerFactory());
		jpaGpsDevice.setNativeExtractor(new SpringNativeJpaExtractor());
		jpaGpsDevice.setMirrorDataChanges(true); //TODO: remove this line since true is the default value
		gps.addGpsDevice(jpaGpsDevice);
		gps.start();
		gps.index();//This is if we need to re-index everything during the application start up.
		//----------

		return compass;
	}
*/
}
