package com.sagg.config;

import java.util.Properties;

import org.springframework.core.env.Environment;

public class Util {
 
	public static void populateProperty(Properties p, Environment env, String propName) {
    	p.put(propName, env.getProperty(propName));
    }

}
