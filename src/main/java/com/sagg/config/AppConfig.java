package com.sagg.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.octo.captcha.service.image.DefaultManageableImageCaptchaService;
import com.octo.captcha.service.image.ImageCaptchaService;
import com.sagg.domain.dao.ActivationDao;
import com.sagg.domain.dao.AdminDao;
import com.sagg.domain.dao.DomainObjectDao;
import com.sagg.domain.dao.StoryDao;
import com.sagg.domain.dao.UserDao;
import com.sagg.domain.dao.jpa.ActivationJpaDao;
import com.sagg.domain.dao.jpa.AdminJpaDao;
import com.sagg.domain.dao.jpa.DomainObjectJpaDao;
import com.sagg.domain.dao.jpa.StoryJpaDao;
import com.sagg.domain.dao.jpa.UserJpaDao;
import com.sagg.service.AdminService;
import com.sagg.service.MailService;
import com.sagg.service.StoryService;
import com.sagg.service.UserService;
import com.sagg.service.impl.AdminServiceImpl;
import com.sagg.service.impl.MailServiceImpl;
import com.sagg.service.impl.StoryServiceImpl;
import com.sagg.service.impl.UserServiceImpl;

@Configuration
//@MBeanExport(registration = RegistrationPolicy.REPLACE_EXISTING)
public class AppConfig {

    public @Bean UserService userService() {
    	return new UserServiceImpl();
    }
    
    public @Bean MailService mailService() {
    	return new MailServiceImpl();
    }

    public @Bean StoryService storyService() {
    	return new StoryServiceImpl();
    }

    public @Bean UserDao userDao() {
    	return new UserJpaDao();
    }

    public @Bean ActivationDao activationDao() {
    	return new ActivationJpaDao();
    }
    
    public @Bean StoryDao storyDao() {
    	return new StoryJpaDao();
    }

    public @Bean DomainObjectDao domainObjectDao() {
    	return new DomainObjectJpaDao();
    }
    
    public @Bean ImageCaptchaService imageCaptchaService() {
    	//TODO: Configure it properly, otherwise sometimes the generation of the image fails complaining about the image being too tall....
    	return new DefaultManageableImageCaptchaService();
    }
    
    public @Bean AdminService adminService() {
    	return new AdminServiceImpl();
    }
    
    public @Bean AdminDao adminDao() {
    	return new AdminJpaDao();
    }
    
}
