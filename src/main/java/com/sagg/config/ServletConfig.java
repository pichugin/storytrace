package com.sagg.config;

import java.util.Properties;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.TransactionException;
import org.springframework.web.servlet.HandlerAdapter;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;
import org.springframework.web.servlet.mvc.SimpleControllerHandlerAdapter;
import org.springframework.web.servlet.mvc.UrlFilenameViewController;
import org.springframework.web.servlet.mvc.annotation.AnnotationMethodHandlerAdapter;
import org.springframework.web.servlet.mvc.support.ControllerClassNameHandlerMapping;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;


@Configuration
//@ComponentScan("com.sagg.web.controller")
@ComponentScan(basePackages = { "com.sagg.web.controller" })
@EnableWebMvc
public class ServletConfig extends WebMvcConfigurerAdapter {
	
    @Override
	public void configureDefaultServletHandling(
			DefaultServletHandlerConfigurer configurer) {
    	configurer.enable();
		//super.configureDefaultServletHandling(configurer);
	}

	/**
     * This bean matches incoming requests to controllers by convention. 
     * For example '/owner/find' is delegated to the OwnerController.find().
     * @see ControllerClassNameHandlerMapping 
     */
//    public @Bean ControllerClassNameHandlerMapping controllerClassNameHandlerMapping() {
//    	ControllerClassNameHandlerMapping ccnhm = new ControllerClassNameHandlerMapping();
//        ccnhm.setDefaultHandler(new UrlFilenameViewController());
//        return ccnhm;
//    }
//    
//    public @Bean HandlerAdapter simpleControllerHandlerAdapter() {
//        return new SimpleControllerHandlerAdapter();
//    }
    

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
    	registry.addResourceHandler("/styles/*.css").addResourceLocations("/", "/styles/").setCachePeriod(1);
    	registry.addResourceHandler("/images/**").addResourceLocations("/", "/images/").setCachePeriod(1);
    	registry.addResourceHandler("/script/*.js").addResourceLocations("/", "/script/").setCachePeriod(1);
    }

    /**
     * This bean processes annotated handler methods, applying
     * myplace-specific PropertyEditors for request parameter binding.
     * It overrides the default AnnotationMethodHandlerAdapter.
     */
//    public @Bean AnnotationMethodHandlerAdapter handlerAdapter() {
//        AnnotationMethodHandlerAdapter adapter = new AnnotationMethodHandlerAdapter();
//        adapter.setOrder(4);
//        return adapter;
//    }

    /**
     * This bean resolves specific types of exceptions to corresponding
     * logical view names for error views. The default behaviour of
     * DispatcherServlet is to propagate all exceptions to the servlet
     * container: this will happen here with all other types of exceptions.
     */
    public @Bean HandlerExceptionResolver exceptionResolver() {
        Properties mappings = new Properties();
        mappings.put(DataAccessException.class.getName(), "dataAccessFailure");
        mappings.put(TransactionException.class.getName(), "dataAccessFailure");

        SimpleMappingExceptionResolver resolver = new SimpleMappingExceptionResolver();
        resolver.setExceptionMappings(mappings);
//        resolver.setOrder(5);

        return resolver;
    }

    /**
     * This bean configures the 'prefix' and 'suffix' properties of
     * InternalResourceViewResolver, * which resolves logical view
     * names returned by Controllers. For example, a logical view name
     * of "vets" will be mapped to "/WEB-INF/jsp/vets.jsp".
     */
    public @Bean ViewResolver viewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setViewClass(JstlView.class);
        resolver.setPrefix("/WEB-INF/jsp/");
        resolver.setSuffix(".jsp");
        return resolver;
    }
    /**
     * Message source for this context, loaded from localized "messages_xx" files.
     * Could also reside in the root application context, as it is generic,
     * but is currently just used within myplace's web tier.
     */
    public @Bean MessageSource messageSource() {
        ResourceBundleMessageSource ms = new ResourceBundleMessageSource();
        ms.setBasename("messages");
        return ms;
    }

}
