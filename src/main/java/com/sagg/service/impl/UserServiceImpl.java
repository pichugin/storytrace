package com.sagg.service.impl;

import java.util.Collection;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.sagg.domain.Activation;
import com.sagg.domain.User;
import com.sagg.domain.dao.ActivationDao;
import com.sagg.domain.dao.DomainObjectDao;
import com.sagg.domain.dao.UserDao;
import com.sagg.service.MailService;
import com.sagg.service.UserService;
import com.sagg.util.Globals;
import com.sagg.util.StringUtil;
import com.sagg.web.form.FeedbackForm;

public class UserServiceImpl implements UserService {
	
	@Autowired private UserDao userDao;
	@Autowired private MailService mailService;
	@Autowired private DomainObjectDao domainObjectDao;
	@Autowired private ActivationDao activationDao;

	public User login(User user) throws Exception {
		if (!StringUtil.isEmpty(user.getEmail())) {
			return userDao.getUserByEmail(user.getEmail());
		} else if (!StringUtil.isEmpty(user.getUsername())) {
			return userDao.getUserByUsername(user.getUsername());
		}
		return null;
	}

	public void logout(User user) throws Exception {
	}

	@Transactional
	public User register(User user) throws Exception {
		user.setActive(false);
		User newUser = userDao.save(user);
		
		String uuid = UUID.randomUUID().toString();
		Activation activation = new Activation(newUser, uuid);
		domainObjectDao.save(activation);
		
		//TODO: Use a template with the email message.
		mailService.sendEmail(user.getEmail(), "Confirmation of registration at storytrace.com", 
				"Dear user,\n\n" +
				"In order to activate your account please click the following link or copy/paste it in the browser's URL field:\n" +
				"http://storytrace.com/main/user/activateAccount?uuid=" + uuid + "\n\n" +
				"Thanks,\n" +
				"The storytrace team.");
		
		return newUser;
	}
	
	@Transactional
	public User activate(String uuid) throws Exception {
		Activation activation = activationDao.find(uuid);
		if (activation == null) {
			return null; //TODO: Show a friendly message explaining the situation.
		}
		User user = (User) domainObjectDao.get(activation.getUser().getId(), User.class);
		if (user == null) {
			return null; //TODO: Show a friendly message explaining the situation.
		}
		user.setActive(true);
		user = (User) domainObjectDao.save(user);
		domainObjectDao.delete(activation.getId(), Activation.class);
		return user;
	}

	@Transactional
	public User save(User user) throws Exception {
		User existingUser = userDao.getUserByEmail(user.getEmail());
		if (existingUser != null && !existingUser.getId().equals(user.getId())) {
			return null;
		}		
		User attachedUser = userDao.getUser(user.getId());
		attachedUser.setEmail(user.getEmail());
		return userDao.save(attachedUser);
	}

	public User getUser(int id) throws Exception {
		return userDao.getUser(id);
	}
	
	public User getUserByEmail(String email) throws Exception {
		return userDao.getUserByEmail(email);
	}

	public User getUserByUsername(String username) throws Exception {
		return userDao.getUserByUsername(username);
	}
	
	@Override
	public Collection<User> getUsers() throws Exception {
		return userDao.getUsers();
	}

	@Transactional
	public User changeEmail(String oldEmail, String newEmail) throws Exception {
		User existingUser = userDao.getUserByEmail(oldEmail);
		if (existingUser == null) {
			return null;
		}
		User userWithNewEmail = userDao.getUserByEmail(newEmail);
		if (userWithNewEmail != null) {
			return null;
		}
		existingUser.setEmail(newEmail);
		return userDao.save(existingUser);
	}
	
	@Transactional
	public User changeUsername(String email, String newUsername) throws Exception {
		User existingUser = userDao.getUserByEmail(email);
		if (existingUser == null) {
			return null;
		}
		
		if (!StringUtil.isEmpty(newUsername) && userDao.getUserByUsername(newUsername) != null) {
			return null;
		}
		
		existingUser.setUsername(StringUtil.isEmpty(newUsername) ? null : newUsername);
		return userDao.save(existingUser);
	}
	
	@Transactional
	public User changePassword(String email, String newPass) throws Exception {
		User existingUser = userDao.getUserByEmail(email);
		if (existingUser == null) {
			return null;
		}
		existingUser.setPassword(newPass);
		return userDao.save(existingUser);
	}

	public void sendPassword(String email) throws Exception {
		User user = userDao.getUserByEmail(email);
		if (user == null) {
			return;
		}
		mailService.sendEmail(user.getEmail(), "Forgotten password retrieval at storytrace.com", 
				"Dear user, \n" +
				"This is a response to your request for retrieving a forgotten password at storytrace.com \n" +
				"Your password is: " + user.getPassword() + " \n\n" +
				"Thanks, \n " +
				"The storytrace team.");
	}

	@Override
	public void sendFeedback(FeedbackForm feedbackForm) throws Exception {
		mailService.sendEmail(Globals.FEEDBACK_EMAIL, "Storytrace Feedback: " + feedbackForm.getSubject(), 
				"From: " + feedbackForm.getEmail() + " \n" +
				"Comment: " + feedbackForm.getComment() + " \n" +
				"URL: " + feedbackForm.getUrl());
	}

}
