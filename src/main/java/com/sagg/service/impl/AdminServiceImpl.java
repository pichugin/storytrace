package com.sagg.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;

import com.sagg.domain.ResultSet;
import com.sagg.domain.dao.AdminDao;
import com.sagg.service.AdminService;
import com.sagg.util.StringUtil;

public class AdminServiceImpl implements AdminService {

	@Autowired private AdminDao adminDao;
	
	public ResultSet executeQuery(String query) throws Exception {
		if (query == null || "".equals(query.trim())) {
			return null;
		}
		query = query.trim();
		
		if (query.startsWith("select") || query.startsWith("SELECT")) {
			Collection<String> columnNames = StringUtil.split(query.toLowerCase(), "select", "from", ",");
			Collection<Object> rows = adminDao.select(query);
			
			return new ResultSet(columnNames, rows);
		}
		//TODO:
//		else if (query.startsWith("update") || query.startsWith("UPDATE")) {
//			adminDao.update(query);
//		}
		
		return null;
	}
	
}
