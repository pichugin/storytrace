package com.sagg.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import org.compass.core.Compass;
import org.compass.core.CompassHit;
import org.compass.core.CompassSession;
import org.compass.core.support.search.CompassSearchCommand;
import org.compass.core.support.search.CompassSearchHelper;
import org.compass.core.support.search.CompassSearchResults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.sagg.domain.dao.DomainObjectDao;
import com.sagg.domain.dao.StoryDao;
import com.sagg.domain.Bookmark;
import com.sagg.domain.Story;
import com.sagg.domain.User;
import com.sagg.domain.Vote;
import com.sagg.exception.DuplicateStoryException;
import com.sagg.service.StoryService;
import com.sagg.util.Globals;

public class StoryServiceImpl implements StoryService {
	
	public static final int PAGE_SIZE = 10;
	public static final long MILLISECS_IN_A_DAY = 1000 * 60 * 60 * 24;

	@Autowired private StoryDao storyDao;
	@Autowired private DomainObjectDao domainObjectDao;
//	@Autowired private Compass compass;
	private Compass compass;

	@Transactional
	public Collection<Story> get(String url) throws Exception {
		return storyDao.find(url);
	}
	
	@Override
	public Collection<Story> getStories() throws Exception {
		return storyDao.getStories();
	}

	@Transactional
	public Story save(Story story) throws Exception {
		Collection<Story> existingStories = storyDao.find(story.getUrl());
		if (!existingStories.isEmpty()) {
			throw new DuplicateStoryException();
		}
		
		story = (Story) domainObjectDao.save(story);
		
		//Index the new object:
		CompassSession compassSession = compass.openSession();
//		compassSession.beginLocalTransaction(); // We are already within a transaction - see the annotation above
		compassSession.save(story);
//		compassSession.commit(); //The Spring's transaction manager should take care of it;
//		compassSession.close();
		//---
		
		return story;
	}
	
	@Transactional
	public Story update(Story story) throws Exception {
		if (story == null) {
			return null;
		}
		
		//Make sure a story with this id exists
		Story existingStory = (Story) domainObjectDao.get(story.getId(), story.getClass());
		if (existingStory == null) {
			return null;
		}
		story.setCreated(existingStory.getCreated());
		story.setVoteDown(existingStory.getVoteDown());
		story.setVoteUp(existingStory.getVoteUp());
		
		//TODO: See if *similar* URLs exist.

		//Make sure we're not changing the url to that of another existing story
		Collection<Story> existingStories = storyDao.find(story.getUrl());
		Iterator<Story> it = existingStories.iterator();
		while (it.hasNext()) {
			if (!it.next().getId().equals(story.getId())) {
				throw new DuplicateStoryException();
			}
		}
		
		story = (Story) domainObjectDao.save(story);
		
		CompassSession compassSession = compass.openSession();
		compassSession.save(story);
		
		return story;
	}
	
	@Transactional
	public void delete(Story story) throws Exception {
		if (story == null) {
			return;
		}
		
		storyDao.delete(story.getId());
		
		CompassSession compassSession = compass.openSession();
		compassSession.delete(story);
	}
	
	public Collection<Story> list(int page, Integer days, String sort) throws Exception {
		Date since = null;
		if (days != null) {
			since = new Date(System.currentTimeMillis() - MILLISECS_IN_A_DAY * days);
		}
		return storyDao.find(page, PAGE_SIZE, since, sort);
	}

	public Story get(Integer id) throws Exception {
		return (Story) domainObjectDao.get(id, Story.class);
	}

	//TODO: redesign this method (see bookmark())
	@Transactional
	public Story vote(User user, Integer storyId, Integer voteDirection) throws Exception {
		Vote vote = user.getVotesMap().get(storyId);
		Story story = get(storyId);
		if (vote == null) {
			vote = new Vote(user, story, voteDirection);
			changeStoryVote(story, voteDirection, true);
			vote = (Vote) domainObjectDao.save(vote);
			user.getVotes().add(vote);
		} else if (voteDirection == 0) {
			changeStoryVote(story, vote.getVoteDirection(), false);
			domainObjectDao.delete(vote.getId(), Vote.class);
			user.getVotes().remove(vote);
		}
		story = (Story) domainObjectDao.save(story);
		return story;
	}
	
	private void changeStoryVote(Story story, Integer voteDirection, boolean increase) {
		if (voteDirection < 0) {
			story.setVoteDown(story.getVoteDown() + (increase ? 1 : -1));
		} else if (voteDirection > 0) {
			story.setVoteUp(story.getVoteUp() + (increase ? 1 : -1));
		}
	}
	
	@Transactional
	public Story flagAsSpam(Integer storyId) throws Exception {
		Story story = get(storyId);
		story.setSpamFlags(story.getSpamFlags() + 1);
		story = (Story) domainObjectDao.save(story);
		return story;
	}

	@Transactional
	public Integer bookmark(User user, Integer storyId, Integer bookmarkId) throws Exception {
		if (bookmarkId == null) {
			Story story = (Story) domainObjectDao.get(storyId, Story.class);
			bookmarkId = domainObjectDao.save(new Bookmark(user, story)).getId();
		} else {
			domainObjectDao.delete(bookmarkId, Bookmark.class);
			bookmarkId = null;
		}
		return bookmarkId;
	}

	@Override
	public Collection<Story> search(String query, Integer page) throws Exception {
		if (query == null || query.length() > Globals.MAX_SEARCH_QUERY_LENGTH) {
			return null;
		}
		if (page == null || page < 0) {
			page = 1;
		}
		query = escapeSpecialChars(query);
		int pageSize = PAGE_SIZE;
		CompassSearchHelper searchHelper = new CompassSearchHelper(compass, pageSize);
		CompassSearchCommand searchCommand = new CompassSearchCommand(query, page - 1);
		CompassSearchResults searchResults = searchHelper.search(searchCommand);
		
		CompassHit[] hits = searchResults.getHits();
		List<Story> foundStories = new ArrayList<Story>(hits.length);
		
		int totalPages = searchResults.getPages().length;
//		for (int i = 0; i < searchResults.getPages().length; i++) {
//			  Page resultPage = searchResults.getPages()[i]; //display a page, for example 1-10, 11-20, 21-30
//		}
		if (totalPages < page) { // If we continued searching we would get results from the beginning - it's kind of an infinite loop.
			return foundStories;
		}
		for (int i = 0; i < hits.length; i++) {
			CompassHit hit = hits[i];
			Object result = hit.getData();
			if (result instanceof Story) {
				foundStories.add(get(((Story) result).getId()));
//				System.out.println("--->" + ((Story) result).getTitle());
			}
		}
		return foundStories;
	}
	
	// Lucene's special characters to be replaced: + - && || ! ( ) { } [ ] ^ " ~ * ? : \
	private final static Pattern LUCENE_SPECIAL_CHARS_PATTERN = Pattern.compile("([\\Q+-(){}[]^*\\?\\E!\"~:]|[&\\|]{2})");
	
	private String escapeSpecialChars(String query) {
		if (query == null) {
			return null;
		} 
		return LUCENE_SPECIAL_CHARS_PATTERN.matcher(query).replaceAll("\\\\$1");
	}

}
