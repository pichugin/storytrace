package com.sagg.service;

public interface MailService {

	void sendEmail(String address, String subject, String text) throws Exception;
	
}
