package com.sagg.service;

import java.util.Collection;

import com.sagg.domain.User;
import com.sagg.web.form.FeedbackForm;

public interface UserService {

	User getUser(int id) throws Exception;
	User getUserByEmail(String email) throws Exception;
	User getUserByUsername(String username) throws Exception;
	Collection<User> getUsers() throws Exception;
	User register(User user) throws Exception;
	User activate(String uuid) throws Exception;
	User save(User user) throws Exception;
	User login(User user) throws Exception;
	void logout(User user) throws Exception;
	User changeEmail(String oldEmail, String newEmail) throws Exception;
	User changeUsername(String email, String newUsername) throws Exception;
	User changePassword(String email, String newPass) throws Exception;
	void sendPassword(String email) throws Exception;
	void sendFeedback(FeedbackForm feedbackForm) throws Exception;

}
