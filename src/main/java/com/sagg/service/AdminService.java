package com.sagg.service;

import com.sagg.domain.ResultSet;

public interface AdminService {

	ResultSet executeQuery(String query) throws Exception;
	
}
