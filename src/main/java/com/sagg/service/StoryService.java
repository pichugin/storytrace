package com.sagg.service;

import java.util.Collection;

import com.sagg.domain.Story;
import com.sagg.domain.User;

public interface StoryService {
	
	Story vote(User user, Integer storyId, Integer voteDirection) throws Exception;
	Story flagAsSpam(Integer storyId) throws Exception;
	Story get(Integer id) throws Exception;
	Integer bookmark(User user, Integer storyId, Integer bookmarkId) throws Exception;
	Collection<Story> getStories() throws Exception;
	Collection<Story> get(String url) throws Exception;
	Collection<Story> search(String query, Integer page) throws Exception;
	Story save(Story story) throws Exception;
	Story update(Story story) throws Exception;
	void delete(Story story) throws Exception;
	Collection<Story> list(int page, Integer days, String sort) throws Exception;
	
}
